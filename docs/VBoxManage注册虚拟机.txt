VBoxManage registervm/unregistervm
registervm命令可以将一个XML格式的虚拟机定义导入VirtualBox。这存在一些限制：不能和已经在VirtualBox中存在的虚拟机冲突；不能带有硬盘或可移动磁盘。可以在定义之前将定义文件放进虚拟机文件夹。
注意
用VBoxManage createvm（见下）创建新的虚拟机时，可以直接指定--register参数避免单独注册。
unregistervm命令注销一个虚拟机。如果同时指定了--delete参数，XML定义文件将被删除。
VBoxManage createvm
这个命令创建一个新的XML虚拟机定义文件。
参数--name <name>是必需的，而且必须指定计算机的名称。由于这个名字默认用作配置文件（扩展名为.xml）和虚拟机文件夹（.VirtualBox/Machines目录的子目录）的文件名，必须符合主机操作系统对文件名的要求。如果虚拟机被重命名，设置文件和文件夹名会自动更改。
然而，如果使用了--basefolder <path>和--settingsfile <filename>参数，XML定义文件将用<filename>命名，虚拟机文件夹用<path>指定。这种情况下，虚拟机的名字改变时，设置文件和虚拟机文件夹的名字不会随之更改。
默认情况下，该命令只创建XML文件而不自动在VirtualBox中注册。为了即刻注册虚拟机，使用--register参数或单独运行VBoxManage registervm。