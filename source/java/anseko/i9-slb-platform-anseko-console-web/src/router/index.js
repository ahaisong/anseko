import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import mainView from '@/views/mainView'

export const constantRouterMap = [
    {
        path: '/redirect',
        component: mainView,
        hidden: true,
        children: [
            {
                path: '/redirect/:path*',
                component: () => import('@/views/redirect/index')
            }
        ]
    },
    {
        path: '/login',
        component: () => import('@/views/loginView'),
        hidden: true
    },
    {
        path: '/auth-redirect',
        component: () => import('@/views/authRedirect'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/pages/404'),
        hidden: true
    },
    {
        path: '/401',
        component: () => import('@/views/pages/401'),
        hidden: true
    },
    {
        path: '',
        component: mainView,
        redirect: 'dashboard',
        children: [
            {
                path: 'dashboard',
                component: () => import('@/views/dashboardView'),
                name: 'Dashboard',
                meta: {title: '启动器', icon: 'dashboard', noCache: true}
            }
        ]
    }
]

export default new Router({
    scrollBehavior: () => ({y: 0}),
    routes: constantRouterMap
})

export const asyncRouterMap = [
    {
        path: '/instance',
        component: mainView,
        children: [
            {
                path: 'listView',
                component: () => import('@/views/instanceListView'),
                name: 'instanceManager_instanceListView',
                meta: {title: '实例管理', icon: 'example'}
            }
        ]
    },
    {
        path: '/instance',
        component: mainView,
        children: [
            {
                path: 'groupView',
                component: () => import('@/views/instanceGroupView'),
                name: 'instanceManager_instanceGroupView',
                meta: {title: '实例组', icon: 'component'}
            }
        ]
    },
    {
        path: '/simulator',
        component: mainView,
        children: [
            {
                path: 'listView',
                component: () => import('@/views/simulatorListView'),
                name: 'simulatorManager_simulatorListView',
                meta: {title: '模拟器管理', icon: 'list'}
            },
            {
                path: 'detailsView',
                component: () => import('@/views/simulatorDetailsView'),
                name: 'simulatorManager_simulatorDetailsView',
                meta: {title: '模拟器详情', noCache: true},
                hidden: true
            }
        ]
    },
    {
        path: '/simulator/displayView',
        component: () => import('@/views/simulatorDisplayView'),
        hidden: true
    },
    {
        path: '/script',
        component: mainView,
        children: [
            {
                path: 'index',
                component: () => import('@/views/scriptView'),
                name: 'scriptManager_index',
                meta: {title: '脚本管理', icon: 'bug'}
            }
        ]
    },
    {
        path: '/user',
        component: mainView,
        children: [
            {
                path: 'index',
                component: () => import('@/views/userView'),
                name: 'userManager_userView',
                meta: {title: '用户管理', icon: 'people'}
            }
        ]
    },
    {path: '*', redirect: '/404', hidden: true}
]
