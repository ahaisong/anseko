import request from '@/utils/request'

export function userLogin(username, password) {
    const data = {
        "username": username,
        "password": password
    }
    return request({
        url: '/userLogin.do_',
        method: 'post',
        params: data
    })
}

export function logout() {
    return request({
        url: '/userLogout.do_',
        method: 'post'
    })
}

export function getUserInfo(userToken) {
    return request({
        url: '/user/getCurrectLoginUserInfo.do_',
        method: 'post',
    })
}

