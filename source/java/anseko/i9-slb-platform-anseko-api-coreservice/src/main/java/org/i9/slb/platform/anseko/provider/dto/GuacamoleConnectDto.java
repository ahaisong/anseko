package org.i9.slb.platform.anseko.provider.dto;

/**
 * Guacamole连接参数
 *
 * @author jiangtao
 * @date 2019.01.16
 */
public class GuacamoleConnectDto implements java.io.Serializable {

    private static final long serialVersionUID = 6810085613411014711L;
    /**
     * vnc协议
     */
    private String protocol;
    /**
     * 连接主机
     */
    private String hostname;
    /**
     * 连接端口
     */
    private Integer port;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
