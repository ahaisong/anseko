package org.i9.slb.platform.anseko.vnc.client.utils;

public class SecurityAuthUtil {

    public static final int ok = 0;
    public static final int failed = 1;

    @Deprecated
    public static final int tooMany = 2;

    public static final int challengeSize = 16;

    public static void encryptChallenge(byte[] challenge, String password) {
        byte[] key = new byte[8];
        for (int i = 0; i < 8 && i < password.length(); i++) {
            key[i] = (byte) password.charAt(i);
        }
        DesCipher desCipher = new DesCipher(key);
        for (int j = 0; j < challengeSize; j += 8) {
            desCipher.encrypt(challenge, j, challenge, j);
        }
    }

    public static void obfuscatePasswd(String password, byte[] obfuscated) {
        for (int i = 0; i < 8; i++) {
            if (i < password.length()) {
                obfuscated[i] = (byte) password.charAt(i);
            } else {
                obfuscated[i] = 0;
            }
        }
        DesCipher desCipher = new DesCipher(obfuscationKey);
        desCipher.encrypt(obfuscated, 0, obfuscated, 0);
    }

    public static String unobfuscatePasswd(byte[] obfuscated) {
        DesCipher desCipher = new DesCipher(obfuscationKey);
        desCipher.decrypt(obfuscated, 0, obfuscated, 0);
        int len;
        for (len = 0; len < 8; len++) {
            if (obfuscated[len] == 0) {
                break;
            }
        }
        char[] plain = new char[len];
        for (int i = 0; i < len; i++) {
            plain[i] = (char) obfuscated[i];
        }
        return new String(plain);
    }

    static byte[] obfuscationKey = {23, 82, 107, 6, 35, 78, 88, 7};
}
