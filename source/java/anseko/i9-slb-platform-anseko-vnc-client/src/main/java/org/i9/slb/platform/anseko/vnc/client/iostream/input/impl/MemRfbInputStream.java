package org.i9.slb.platform.anseko.vnc.client.iostream.input.impl;

import org.i9.slb.platform.anseko.vnc.client.exception.IoStreamException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;

/**
 * 内存式InputStream
 *
 * @author jiangtao
 * @date 2019年1月24日 09:35:56
 */
public class MemRfbInputStream extends RfbInputStream {

    public MemRfbInputStream(byte[] data, int offset, int len) {
        buf = data;
        ptr = offset;
        end = offset + len;
    }

    @Override
    public int pos() {
        return ptr;
    }

    @Override
    protected int overrun(int itemSize, int nItems) {
        throw new IoStreamException("MemInputStream不支持此方法");
    }
}
