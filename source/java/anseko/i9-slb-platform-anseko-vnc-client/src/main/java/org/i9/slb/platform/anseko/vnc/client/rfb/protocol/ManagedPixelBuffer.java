package org.i9.slb.platform.anseko.vnc.client.rfb.protocol;

public class ManagedPixelBuffer extends PixelBuffer {

    public void setSize(int w, int h) {
        width_ = w;
        height_ = h;
        checkDataSize();
    }

    @Override
    public void setPixelFormat(PixelFormat pixelFormat) {
        super.setPixelFormat(pixelFormat);
        checkDataSize();
    }

    public int dataLen() {
        return area() * (getPixelFormat().bpp / 8);
    }

    final void checkDataSize() {
        if (data == null || data.length < dataLen())
            data = new byte[dataLen()];
    }
}
