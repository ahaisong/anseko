package org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl;

import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Hextile;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.VncClientRfbProtocolDecoder;

/**
 * vnc客户端rfb协议解码器（Hextile）
 *
 * @author r12
 * @date 2019年2月1日 10:26:55
 */
public class VncClientRfbProtocolHextileDecoder extends VncClientRfbProtocolDecoder {

    private static final int BPP = 8;

    private VncClientMessageReader reader;

    public VncClientRfbProtocolHextileDecoder(VncClientMessageReader reader) {
        this.reader = reader;
    }

    static final int readPixel(RfbInputStream inputStream) {
        return inputStream.readU8();
    }

    @Override
    public void readRect(int x, int y, int width, int height, VncClientMessageHandler handler) {
        RfbInputStream inputStream = reader.getInputStream();
        byte[] buf = reader.getImageBuf(16 * 16 * 4, 0);

        int bg = 0;
        int fg = 0;

        for (int ty = y; ty < y + height; ty += 16) {

            int th = Math.min(y + height - ty, 16);

            for (int tx = x; tx < x + width; tx += 16) {

                int tw = Math.min(x + width - tx, 16);

                int tileType = inputStream.readU8();

                if ((tileType & Hextile.raw) != 0) {
                    inputStream.readBytes(buf, 0, tw * th * (BPP / 8));
                    handler.imageRect(tx, ty, tw, th, buf, 0);
                    continue;
                }

                if ((tileType & Hextile.bgSpecified) != 0) {
                    bg = readPixel(inputStream);
                }

                int len = tw * th;
                int ptr = 0;
                while (len-- > 0) {
                    buf[ptr++] = (byte) bg;
                }
                if ((tileType & Hextile.fgSpecified) != 0) {
                    fg = readPixel(inputStream);
                }

                if ((tileType & Hextile.anySubrects) != 0) {
                    int nSubrects = inputStream.readU8();

                    for (int i = 0; i < nSubrects; i++) {

                        if ((tileType & Hextile.subrectsColoured) != 0) {
                            fg = readPixel(inputStream);
                        }

                        int xy = inputStream.readU8();
                        int wh = inputStream.readU8();
                        int sx = ((xy >> 4) & 15);
                        int sy = (xy & 15);
                        int sw = ((wh >> 4) & 15) + 1;
                        int sh = (wh & 15) + 1;
                        ptr = sy * tw + sx;
                        int rowAdd = tw - sw;
                        while (sh-- > 0) {
                            len = sw;
                            while (len-- > 0) {
                                buf[ptr++] = (byte) fg;
                            }
                            ptr += rowAdd;
                        }
                    }
                }
                handler.imageRect(tx, ty, tw, th, buf, 0);
            }
        }
    }
}
