package org.i9.slb.platform.anseko.vnc.client.rfb.protocol;

/**
 * vnc客户端光标处理类
 */
public class Cursor extends ManagedPixelBuffer {

    @Override
    public void setSize(int w, int h) {
        super.setSize(w, h);
        if (mask == null || mask.length < maskLen()) {
            mask = new byte[maskLen()];
        }
    }

    public int maskLen() {
        return (width() + 7) / 8 * height();
    }

    public int hotspotX, hotspotY;

    public byte[] mask;
}
