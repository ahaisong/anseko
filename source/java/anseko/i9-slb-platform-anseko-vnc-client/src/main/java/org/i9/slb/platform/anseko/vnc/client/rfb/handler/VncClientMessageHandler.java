package org.i9.slb.platform.anseko.vnc.client.rfb.handler;

import org.i9.slb.platform.anseko.vnc.client.rfb.connection.ConnectionParameter;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.PixelFormat;

/**
 * vnc客户端消息处理基类
 *
 * @author r12
 * @date 2019年1月31日 13:32:05
 */
public abstract class VncClientMessageHandler {

    private ConnectionParameter connectionParameter;

    public VncClientMessageHandler() {
        connectionParameter = new ConnectionParameter();
    }

    /**
     * 记录desktop大小
     *
     * @param width
     * @param height
     */
    public void setDesktopSize(int width, int height) {
        connectionParameter.setWidth(width);
        connectionParameter.setHeight(height);
    }

    /**
     * 获取vnc连接参数
     *
     * @return
     */
    public ConnectionParameter getConnectionParameter() {
        return connectionParameter;
    }

    public void setPixelFormat(PixelFormat pixelFormat) {
        connectionParameter.setPixelFormat(pixelFormat);
    }

    /**
     * 设置desktopName
     *
     * @param desktopName
     */
    public void setDesktopName(String desktopName) {
        connectionParameter.setDesktopName(desktopName);
    }

    public abstract void setCursor(int hotspotX, int hotspotY, int w, int h, byte[] data, byte[] mask);

    public abstract void serverInit();

    public abstract void framebufferUpdateStart();

    public abstract void framebufferUpdateEnd();

    public abstract void beginRect(int x, int y, int w, int h, int encoding);

    public abstract void endRect(int x, int y, int w, int h, int encoding);

    public abstract void setColourMapEntries(int firstColour, int nColours, int[] rgbs);

    public abstract void bell();

    public abstract void serverCutText(String str);

    public abstract void fillRect(int x, int y, int w, int h, int pix);

    public abstract void imageRect(int x, int y, int w, int h, byte[] pix, int offset);

    public abstract void copyRect(int x, int y, int w, int h, int srcX, int srcY);
}
