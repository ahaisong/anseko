package org.i9.slb.platform.anseko.vnc.client.rfb.adapter;

import org.apache.log4j.Logger;
import org.i9.slb.platform.anseko.vnc.client.exception.RfbProtocolException;
import org.i9.slb.platform.anseko.vnc.client.exception.VncAuthFailureException;
import org.i9.slb.platform.anseko.vnc.client.exception.VncConnFailureException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Keysyms;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.SecurityTypes;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.impl.VncClientMessageReaderV3;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.writer.VncClientMessageWriter;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.writer.impl.VncClientMessageWriterV3;
import org.i9.slb.platform.anseko.vnc.client.rfb.security.VncClientSecurity;
import org.i9.slb.platform.anseko.vnc.client.rfb.security.impl.VncClientSecurityAuth;
import org.i9.slb.platform.anseko.vnc.client.rfb.security.impl.VncClientSecurityNone;
import org.i9.slb.platform.anseko.vnc.client.utils.UnicodeToKeysym;

/**
 * vnc客户端消息处理（适配器）
 *
 * @author r12
 * @date 2019年1月31日 13:40:49
 */
public abstract class VncClientMessageHandlerAdapter extends VncClientMessageHandler {

    private static final Logger LOGGER = Logger.getLogger(VncClientMessageHandlerAdapter.class);

    public static final int RFBSTATE_UNINITIALISED = 0;
    public static final int RFBSTATE_PROTOCOL_VERSION = 1;
    public static final int RFBSTATE_SECURITY_TYPES = 2;
    public static final int RFBSTATE_SECURITY = 3;
    public static final int RFBSTATE_SECURITY_RESULT = 4;
    public static final int RFBSTATE_INITIALISATION = 5;
    public static final int RFBSTATE_NORMAL = 6;
    public static final int RFBSTATE_INVALID = 7;
    public static final int maxSecTypes = 8;

    /**
     * 服务器名称
     */
    private String serverName;
    /**
     * rfb协议输入流
     */
    private RfbInputStream inputStream;
    /**
     * rfb协议输出流
     */
    private RfbOutputStream outputStream;
    /**
     * vnc客户端消息读操作
     */
    private VncClientMessageReader reader;
    /**
     * vnc客户端消息写操作
     */
    private VncClientMessageWriter writer;
    /**
     * vnc客户端安全认证
     */
    private VncClientSecurity security;

    private int nSecTypes;
    private int[] secTypes;
    private int state_;

    private boolean useProtocol3_3;
    private boolean clientSecTypeOrder;

    private boolean shared;

    private boolean sameMachine;

    private int currentEncoding;
    private int lastUsedEncoding;

    public VncClientMessageHandlerAdapter() {
        this.state_ = RFBSTATE_UNINITIALISED;
        this.secTypes = new int[maxSecTypes];
        this.currentEncoding = Encodings.ZRLE;
        this.lastUsedEncoding = Encodings.MAX;
        this.initSecType();
    }

    public void initSecType() {
        this.addSecType(SecurityTypes.none);
        this.addSecType(SecurityTypes.vncAuth);
    }

    public VncClientSecurity getSecurity(int secType) {
        switch (secType) {
            case SecurityTypes.none:
                return new VncClientSecurityNone();
            case SecurityTypes.vncAuth:
                return new VncClientSecurityAuth(this.getConnectionParameter().getPassword());
            default:
                throw new RfbProtocolException("Unsupported secType?");
        }
    }

    protected int getCurrentEncoding() {
        return currentEncoding;
    }

    public void setCurrentEncoding(int currentEncoding) {
        this.currentEncoding = currentEncoding;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public void setRfbStreams(RfbInputStream inputStream, RfbOutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void addSecType(int secType) {
        if (nSecTypes == maxSecTypes) {
            throw new RfbProtocolException("too many security types");
        }
        secTypes[nSecTypes++] = secType;
    }

    public void initialiseProtocol() {
        state_ = RFBSTATE_PROTOCOL_VERSION;
    }

    public void processMessage() {
        LOGGER.debug("vnc接收处理消息状态, state_ : " + state_);
        switch (state_) {
            case RFBSTATE_PROTOCOL_VERSION:
                this.processVersionMessage();
                break;
            case RFBSTATE_SECURITY_TYPES:
                this.processSecurityTypesMessage();
                break;
            case RFBSTATE_SECURITY:
                this.processSecurityMessage();
                break;
            case RFBSTATE_SECURITY_RESULT:
                this.processSecurityResultMessage();
                break;
            case RFBSTATE_INITIALISATION:
                this.processInitMessage();
                break;
            case RFBSTATE_NORMAL:
                this.reader().readMessage();
                break;
            case RFBSTATE_UNINITIALISED:
                throw new RfbProtocolException("CConnection.processMessage: not initialised yet?");
            default:
                throw new RfbProtocolException("CConnection.processMessage: invalid state");
        }
    }

    /**
     * 安全认证成功后调用方法
     */
    public abstract void securityAuthSuccess();

    synchronized protected void checkEncodings() {
        if (this.isEncodingChange() && state() == RFBSTATE_NORMAL) {
            LOGGER.info("Using " + Encodings.valueOf(currentEncoding) + " encoding");
            writer().writeSetEncodings(currentEncoding, true);
            this.setEncodingChange(false);
        }
    }

    public VncClientMessageReader reader() {
        if (this.reader == null) {
            LOGGER.debug("vnc客户端vncClientMessageReader对象未初始化");
        }
        return reader;
    }

    public VncClientMessageWriter writer() {
        if (this.writer == null) {
            LOGGER.debug("vnc客户端vncClientMessageWriter对象未初始化");
        }
        return this.writer;
    }

    public RfbInputStream getInputStream() {
        return inputStream;
    }

    public RfbOutputStream getOutputStream() {
        return outputStream;
    }

    public String getServerName() {
        return serverName;
    }

    public int state() {
        return state_;
    }

    protected void setState(int state) {
        state_ = state;
    }

    /**
     * 处理读取vnc服务器版本号消息
     */
    private void processVersionMessage() {
        LOGGER.debug("processing version message");

        if (!getConnectionParameter().readVersion(inputStream)) {
            this.setState(RFBSTATE_INVALID);
            throw new RfbProtocolException("reading version failed: not an RFB server?");
        }

        int majorVersion = this.getConnectionParameter().getMajorVersion();
        int minorVersion = this.getConnectionParameter().getMinorVersion();
        LOGGER.info("vnc服务器支持rfb协议版本号为 : " + majorVersion + "." + minorVersion);

        // The only official RFB protocol versions are currently 3.3, 3.7 and 3.8
        if (this.getConnectionParameter().beforeVersion(3, 3)) {
            LOGGER.error("vnc服务器提供了不支持的RFB协议版本 : " + majorVersion + "." + minorVersion);
            this.setState(RFBSTATE_INVALID);
            throw new RfbProtocolException("vnc服务器提供了不支持的RFB协议版本 : " + majorVersion + "." + minorVersion);
        } else if (useProtocol3_3 || this.getConnectionParameter().beforeVersion(3, 7)) {
            this.getConnectionParameter().setVersion(3, 3);
        } else if (this.getConnectionParameter().afterVersion(3, 8)) {
            this.getConnectionParameter().setVersion(3, 8);
        }

        this.getConnectionParameter().writeVersion(outputStream);
        this.setState(RFBSTATE_SECURITY_TYPES);
        LOGGER.info("vnc服务器使用RFB协议版本 " + majorVersion + "." + minorVersion);
    }

    /**
     * 处理vnc服务器安全认证类型消息
     */
    private void processSecurityTypesMessage() {
        LOGGER.debug("processing security types message");

        int secType = SecurityTypes.invalid;
        int majorVersion = this.getConnectionParameter().getMajorVersion();
        int minorVersion = this.getConnectionParameter().getMinorVersion();

        if (majorVersion == 3 && minorVersion == 3) {
            secType = inputStream.readU32();
            if (secType == SecurityTypes.invalid) {
                throwConnFailedException();
            } else if (secType == SecurityTypes.none || secType == SecurityTypes.vncAuth) {
                int j;
                for (j = 0; j < nSecTypes; j++) {
                    if (secTypes[j] == secType) {
                        break;
                    }
                }
                if (j == nSecTypes) {
                    secType = SecurityTypes.invalid;
                }
            } else {
                LOGGER.error("Unknown 3.3 security type " + secType);
                throw new RfbProtocolException("Unknown 3.3 security type");
            }
        } else {
            // 3.7 server will offer us a list
            int nServerSecTypes = inputStream.readU8();
            if (nServerSecTypes == 0) {
                throwConnFailedException();
            }

            int secTypePos = nSecTypes;
            for (int i = 0; i < nServerSecTypes; i++) {
                int serverSecType = inputStream.readU8();
                LOGGER.debug("vnc服务器安全认证类型 : " + SecurityTypes.valueOf(serverSecType) + "(" + serverSecType + ")");

                // If we haven't already chosen a secType, try this one
                if (secType == SecurityTypes.invalid || clientSecTypeOrder) {
                    for (int j = 0; j < nSecTypes; j++) {
                        if (secTypes[j] == serverSecType && j < secTypePos) {
                            secType = secTypes[j];
                            secTypePos = j;
                            break;
                        }
                    }
                }
            }

            if (secType != SecurityTypes.invalid) {
                outputStream.writeU8(secType);
                outputStream.flush();
                LOGGER.debug("vnc服务器选择安全类型 : " + SecurityTypes.valueOf(secType) + "(" + secType + ")");
            }
        }

        if (secType == SecurityTypes.invalid) {
            state_ = RFBSTATE_INVALID;
            LOGGER.error("No matching security types");
            throw new RfbProtocolException("No matching security types");
        }

        this.setState(RFBSTATE_SECURITY);
        security = getSecurity(secType);
        processSecurityMessage();
    }

    private void processSecurityMessage() {
        LOGGER.debug("processing security message");
        boolean b = security.processMessage(this);
        if (!b) {
            throwAuthFailureException();
        } else {
            setState(RFBSTATE_SECURITY_RESULT);
            processSecurityResultMessage();
        }
    }

    private void processSecurityResultMessage() {
        LOGGER.debug("processing security result message");
        int result;
        if (this.getConnectionParameter().beforeVersion(3, 8) && security.getType() == SecurityTypes.none) {
            result = SecurityTypes.resultOK;
        } else {
            result = inputStream.readU32();
        }
        switch (result) {
            case SecurityTypes.resultOK:
                securityCompleted();
                break;
            case SecurityTypes.resultFailed:
                LOGGER.debug("auth failed");
                throwAuthFailureException();
            case SecurityTypes.resultTooMany:
                LOGGER.debug("auth failed - too many tries");
                throwAuthFailureException();
            default:
                LOGGER.error("unknown security result");
                throwAuthFailureException();
        }
    }

    void processInitMessage() {
        LOGGER.debug("reading server initialisation");
        reader.readServerInit();
    }

    void throwAuthFailureException() {
        String reason;
        int majorVersion = this.getConnectionParameter().getMajorVersion();
        int minorVersion = this.getConnectionParameter().getMinorVersion();
        LOGGER.debug("state=" + state() + ", ver=" + majorVersion + "." + minorVersion);
        if (state() == RFBSTATE_SECURITY_RESULT && !this.getConnectionParameter().beforeVersion(3, 8)) {
            reason = inputStream.readString();
        } else {
            reason = "Authentication failure";
        }
        state_ = RFBSTATE_INVALID;
        LOGGER.error(reason);
        throw new VncAuthFailureException(reason);
    }

    void throwConnFailedException() {
        state_ = RFBSTATE_INVALID;
        String reason = inputStream.readString();
        throw new VncConnFailureException(reason);
    }

    /**
     * vnc安全认证完成
     */
    void securityCompleted() {
        state_ = RFBSTATE_INITIALISATION;
        reader = new VncClientMessageReaderV3(this, inputStream);
        writer = new VncClientMessageWriterV3(this.getConnectionParameter(), outputStream);
        LOGGER.debug("vnc服务器认证成功");
        this.securityAuthSuccess();
        writer.writeClientInit(shared);
    }

    /**
     * 发送键盘命令事件
     *
     * @param key
     * @param down
     */
    public void writeKeyEvent(int key, boolean down) {
        if (this.writer() != null) {
            this.writer().writeKeyEvent(key, down);
        }
    }

    /**
     * 发送鼠标命令事件
     *
     * @param x
     * @param y
     * @param buttonMask
     */
    public void writePointerEvent(int x, int y, int buttonMask) {
        if (this.writer() != null) {
            this.writer().writePointerEvent(x, y, buttonMask);
        }
    }

    /**
     * 发送字符串命令事件
     *
     * @param str
     */
    public void writeCharStrEvent(String str, boolean isCR) {
        if (this.writer() != null) {
            for (int i = 0; i < str.length(); i++) {
                char c = str.charAt(i);
                int keysym = UnicodeToKeysym.translate(c);
                this.writeKeyEvent(keysym, true);
            }
            if (isCR) {
                int keysym = Keysyms.Return;
                this.writeKeyEvent(keysym, true);
            }
        }
    }

    public abstract boolean isEncodingChange();

    public abstract void setEncodingChange(boolean encodingChange);
}
