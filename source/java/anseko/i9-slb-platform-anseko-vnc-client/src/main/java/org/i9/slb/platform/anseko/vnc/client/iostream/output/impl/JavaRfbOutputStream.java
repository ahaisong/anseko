package org.i9.slb.platform.anseko.vnc.client.iostream.output.impl;

import org.i9.slb.platform.anseko.vnc.client.exception.IoStreamException;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;

import java.io.IOException;
import java.io.OutputStream;

public class JavaRfbOutputStream extends RfbOutputStream {

    private static final int DEFAULT_BUF_SIZE = 16384;

    private static final int MIN_BULK_SIZE = 1024;

    private OutputStream outputStream;

    private int ptrOffset;

    private int bufSize;

    public JavaRfbOutputStream(java.io.OutputStream outputStream, int bufSize) {
        this.outputStream = outputStream;
        this.bufSize = bufSize;
        this.buf = new byte[bufSize];
        this.ptr = 0;
        this.end = bufSize;
    }

    public JavaRfbOutputStream(java.io.OutputStream jos) {
        this(jos, DEFAULT_BUF_SIZE);
    }

    @Override
    public void writeBytes(byte[] data, int offset, int length) {
        if (length < MIN_BULK_SIZE) {
            super.writeBytes(data, offset, length);
            return;
        }
        flush();
        try {
            this.outputStream.write(data, offset, length);
        } catch (IOException e) {
            throw new IoStreamException("writeBytes写入异常");
        }
        ptrOffset += length;
    }

    @Override
    public void flush() {
        try {
            this.outputStream.write(this.buf, 0, ptr);
        } catch (IOException e) {
            throw new IoStreamException("flush写入异常");
        }
        ptrOffset += ptr;
        ptr = 0;
    }

    @Override
    public int length() {
        return ptrOffset + ptr;
    }

    @Override
    protected int overrun(int itemSize, int nItems) {
        if (itemSize > bufSize) {
            throw new IoStreamException("超过最大项大小");
        }
        flush();
        if (itemSize * nItems > end) {
            nItems = end / itemSize;
        }
        return nItems;
    }

}
