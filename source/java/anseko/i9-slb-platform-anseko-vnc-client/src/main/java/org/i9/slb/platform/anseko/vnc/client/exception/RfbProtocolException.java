package org.i9.slb.platform.anseko.vnc.client.exception;

/**
 * rfb协议异常
 *
 * @author jiangtao
 * @date 2019-01-22
 */
public class RfbProtocolException extends RuntimeException {

    private static final long serialVersionUID = -178104721526226061L;

    public RfbProtocolException(String message) {
        super(message);
    }
}
