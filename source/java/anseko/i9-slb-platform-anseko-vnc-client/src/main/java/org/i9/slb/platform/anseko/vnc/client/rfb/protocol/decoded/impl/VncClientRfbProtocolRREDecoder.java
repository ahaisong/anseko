package org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl;

import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.VncClientRfbProtocolDecoder;

/**
 * vnc客户端rfb协议解码器（RRE）
 *
 * @author r12
 * @date 2019年2月1日 10:26:55
 */
public class VncClientRfbProtocolRREDecoder extends VncClientRfbProtocolDecoder {

    private VncClientMessageReader reader;

    public VncClientRfbProtocolRREDecoder(VncClientMessageReader reader) {
        this.reader = reader;
    }

    static final int BPP = 8;

    static final int readPixel(RfbInputStream inputStream) {
        return inputStream.readU8();
    }

    @Override
    public void readRect(int x, int y, int width, int height, VncClientMessageHandler handler) {
        RfbInputStream inputStream = reader.getInputStream();
        int nSubrects = inputStream.readU32();
        int bg = readPixel(inputStream);
        handler.fillRect(x, y, width, height, bg);

        for (int i = 0; i < nSubrects; i++) {
            int pix = readPixel(inputStream);
            int sx = inputStream.readU16();
            int sy = inputStream.readU16();
            int sw = inputStream.readU16();
            int sh = inputStream.readU16();
            handler.fillRect(x + sx, y + sy, sw, sh, pix);
        }
    }
}
