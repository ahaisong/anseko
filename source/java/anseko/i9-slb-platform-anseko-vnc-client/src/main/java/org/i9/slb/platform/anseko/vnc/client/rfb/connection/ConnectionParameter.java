package org.i9.slb.platform.anseko.vnc.client.rfb.connection;

import org.i9.slb.platform.anseko.vnc.client.exception.RfbProtocolException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.PixelFormat;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.encoded.VncClientRfbProtocolEncoder;

/**
 * vnc连接参数类
 *
 * @author r12
 * @date 2019年2月1日 10:15:25
 */
public class ConnectionParameter {

    private boolean useCopyRect;

    private boolean supportsLocalCursor;

    private boolean supportsDesktopResize;

    private PixelFormat pixelFormat;

    private int nEncodings;

    private int[] encodings;

    private int currentEncoding;

    private int majorVersion;

    private int minorVersion;

    private int width;

    private int height;

    private String desktopName;

    /**
     * vnc客户端认证用户名
     */
    private String username;
    /**
     * vnc客户端认证密码
     */
    private String password;

    public boolean readVersion(RfbInputStream inputStream) {
        byte[] b = new byte[12];
        inputStream.readBytes(b, 0, 12);
        try {
            if ((b[0] != 'R') || (b[1] != 'F') || (b[2] != 'B') || (b[3] != ' ')
                    || (b[4] < '0') || (b[4] > '9') || (b[5] < '0') || (b[5] > '9')
                    || (b[6] < '0') || (b[6] > '9') || (b[7] != '.')
                    || (b[8] < '0') || (b[8] > '9') || (b[9] < '0') || (b[9] > '9')
                    || (b[10] < '0') || (b[10] > '9') || (b[11] != '\n')) {
                return false;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // On IE 5.0, the above test causes an exception if executed unmodified.
            // Wrapping it inside a try/catch block inputStream the cleanest way of fixing it.
            return false;
        }
        majorVersion = (b[4] - '0') * 100 + (b[5] - '0') * 10 + (b[6] - '0');
        minorVersion = (b[8] - '0') * 100 + (b[9] - '0') * 10 + (b[10] - '0');
        return true;
    }

    public void writeVersion(RfbOutputStream outputStream) {
        byte[] buf = new byte[12];
        buf[0] = (byte) 'R';
        buf[1] = (byte) 'F';
        buf[2] = (byte) 'B';
        buf[3] = (byte) ' ';
        buf[4] = (byte) ('0' + (majorVersion / 100) % 10);
        buf[5] = (byte) ('0' + (majorVersion / 10) % 10);
        buf[6] = (byte) ('0' + majorVersion % 10);
        buf[7] = (byte) '.';
        buf[8] = (byte) ('0' + (minorVersion / 100) % 10);
        buf[9] = (byte) ('0' + (minorVersion / 10) % 10);
        buf[10] = (byte) ('0' + minorVersion % 10);
        buf[11] = (byte) '\n';
        outputStream.writeBytes(buf, 0, 12);
        outputStream.flush();
    }

    public void setVersion(int majorVersion, int minorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }

    public boolean isVersion(int majorVersion, int minorVersion) {
        return this.majorVersion == majorVersion && this.minorVersion == minorVersion;
    }

    public boolean beforeVersion(int majorVersion, int minorVersion) {
        return (this.majorVersion < majorVersion ||
                (this.majorVersion == majorVersion && this.minorVersion < minorVersion));
    }

    public boolean afterVersion(int majorVersion, int minorVersion) {
        return !beforeVersion(majorVersion, minorVersion + 1);
    }

    public PixelFormat getPixelFormat() {
        return pixelFormat;
    }

    public void setPixelFormat(PixelFormat pixelFormat) {
        this.pixelFormat = pixelFormat;
        if (pixelFormat.bpp != 8 && pixelFormat.bpp != 16 && pixelFormat.bpp != 32) {
            throw new RfbProtocolException("setPixelFormat: not 8, 16 or 32 bpp?");
        }
    }

    public int currentEncoding() {
        return currentEncoding;
    }

    public int nEncodings() {
        return nEncodings;
    }

    public int[] encodings() {
        return encodings;
    }

    public void setEncodings(int nEncodings, int[] encodings) {
        if (nEncodings > this.nEncodings) {
            this.encodings = new int[nEncodings];
        }

        this.nEncodings = nEncodings;
        this.useCopyRect = false;
        this.supportsLocalCursor = false;
        this.supportsDesktopResize = false;
        this.currentEncoding = Encodings.RAW;

        for (int i = nEncodings - 1; i >= 0; i--) {
            this.encodings[i] = encodings[i];
            if (encodings[i] == Encodings.COPY_RECT) {
                useCopyRect = true;
            } else if (encodings[i] == Encodings.pseudoEncodingCursor) {
                supportsLocalCursor = true;
            } else if (encodings[i] == Encodings.pseudoEncodingDesktopSize) {
                supportsDesktopResize = true;
            } else if (encodings[i] <= Encodings.MAX && VncClientRfbProtocolEncoder.supported(encodings[i])) {
                currentEncoding = encodings[i];
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isUseCopyRect() {
        return useCopyRect;
    }

    public void setUseCopyRect(boolean useCopyRect) {
        this.useCopyRect = useCopyRect;
    }

    public boolean isSupportsLocalCursor() {
        return supportsLocalCursor;
    }

    public void setSupportsLocalCursor(boolean supportsLocalCursor) {
        this.supportsLocalCursor = supportsLocalCursor;
    }

    public boolean isSupportsDesktopResize() {
        return supportsDesktopResize;
    }

    public void setSupportsDesktopResize(boolean supportsDesktopResize) {
        this.supportsDesktopResize = supportsDesktopResize;
    }

    public int getnEncodings() {
        return nEncodings;
    }

    public void setnEncodings(int nEncodings) {
        this.nEncodings = nEncodings;
    }

    public int[] getEncodings() {
        return encodings;
    }

    public void setEncodings(int[] encodings) {
        this.encodings = encodings;
    }

    public int getCurrentEncoding() {
        return currentEncoding;
    }

    public void setCurrentEncoding(int currentEncoding) {
        this.currentEncoding = currentEncoding;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(int minorVersion) {
        this.minorVersion = minorVersion;
    }

    public String getDesktopName() {
        return desktopName;
    }

    public void setDesktopName(String desktopName) {
        this.desktopName = desktopName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
