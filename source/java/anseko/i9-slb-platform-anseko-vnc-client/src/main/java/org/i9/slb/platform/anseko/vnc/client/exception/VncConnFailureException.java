package org.i9.slb.platform.anseko.vnc.client.exception;

import com.sun.istack.internal.NotNull;

/**
 * vnc连接错误异常
 *
 * @author r12
 * @date 2019/1/24 10:25
 */
public class VncConnFailureException extends RuntimeException {

    private static final long serialVersionUID = 8683311475278888799L;

    private String message;

    public VncConnFailureException(@NotNull String message) {
        super(message);
    }
}
