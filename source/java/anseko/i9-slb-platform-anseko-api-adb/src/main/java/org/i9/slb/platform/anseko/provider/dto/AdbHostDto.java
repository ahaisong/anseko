package org.i9.slb.platform.anseko.provider.dto;

/**
 * 命令回调参数
 *
 * @author jiangtao
 * @date 2019-01-16
 */
public class AdbHostDto implements java.io.Serializable {

    private static final long serialVersionUID = 7973607896256828761L;
    /**
     * 主机地址
     */
    private String host;
    /**
     * 端口
     */
    private Integer port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return host + ":" + port;
    }
}
