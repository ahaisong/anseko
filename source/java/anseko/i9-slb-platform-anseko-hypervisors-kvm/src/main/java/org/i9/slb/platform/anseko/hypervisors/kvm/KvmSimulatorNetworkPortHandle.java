package org.i9.slb.platform.anseko.hypervisors.kvm;

import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorNetworkPortHandle;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * kvm 模拟器端口映射工具类
 *
 * @author r12
 * @date 2019-03-26
 */
public class KvmSimulatorNetworkPortHandle extends SimulatorNetworkPortHandle {

    public KvmSimulatorNetworkPortHandle(String simulatorId, IDownStreamRemoteCommand downStreamRemoteCommand) {
        super(simulatorId, downStreamRemoteCommand);
    }

    /**
     * 进行端口映射操作
     *
     * @param typeStr
     * @param remoteAddress
     * @param sport
     * @param remoteAddress
     * @param dport
     */
    @Override
    public void executePortMappingCommand(String typeStr, String localAddress, Integer sport, String remoteAddress, Integer dport) {
        List<ShellCommandParamDto> shellCommandParamDtos = new ArrayList<ShellCommandParamDto>();
        for (String command : new String[]{
                "iptables -t nat -A PREROUTING -d " + remoteAddress + " -p tcp -m tcp --dport " + dport + " -j DNAT --to " + localAddress + ":" + sport,
                "iptables -t nat -A OUTPUT -s " + remoteAddress + " -p tcp -m tcp --dport " + dport + " -j DNAT --to " + localAddress + ":" + sport
        }) {
            ShellCommandParamDto shellCommandParamDto = new ShellCommandParamDto();
            shellCommandParamDto.setCommandId(UUIDUtil.generateUUID());
            shellCommandParamDto.setCommandLine(command);
            shellCommandParamDtos.add(shellCommandParamDto);
        }
        this.downStreamRemoteShellCommandExecuteBatch("映射端口", shellCommandParamDtos);
    }
}
