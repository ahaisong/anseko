package org.i9.slb.platform.anseko.vncserver.servlet;

import org.apache.commons.lang3.StringUtils;
import org.glyptodon.guacamole.GuacamoleException;
import org.glyptodon.guacamole.net.GuacamoleSocket;
import org.glyptodon.guacamole.net.GuacamoleTunnel;
import org.glyptodon.guacamole.net.InetGuacamoleSocket;
import org.glyptodon.guacamole.net.SimpleGuacamoleTunnel;
import org.glyptodon.guacamole.protocol.ConfiguredGuacamoleSocket;
import org.glyptodon.guacamole.protocol.GuacamoleClientInformation;
import org.glyptodon.guacamole.protocol.GuacamoleConfiguration;
import org.glyptodon.guacamole.servlet.GuacamoleHTTPTunnelServlet;
import org.glyptodon.guacamole.servlet.GuacamoleSession;
import org.i9.slb.platform.anseko.provider.dto.GuacamoleConnectDto;
import org.i9.slb.platform.anseko.vncserver.config.GuacamoleRemoteServiceProperties;
import org.i9.slb.platform.anseko.vncserver.remote.CoreserviceRemoteService;
import org.i9.slb.platform.anseko.vncserver.remote.SpringBeanService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class BasicGuacamoleTunnelServlet extends GuacamoleHTTPTunnelServlet {

    private static final long serialVersionUID = -6257946949350811664L;

    @Override
    protected GuacamoleTunnel doConnect(HttpServletRequest request) throws GuacamoleException {
        HttpSession httpSession = request.getSession(true);
        String simulatorId = (String) httpSession.getAttribute("simulatorId");
        String chk = (String) httpSession.getAttribute("chk");
        GuacamoleConnectDto guacamoleConnectDto = (GuacamoleConnectDto) httpSession.getAttribute("guacamoleConnectDto");
        if (guacamoleConnectDto == null || "true".equals(chk)) {
            CoreserviceRemoteService coreserviceRemoteService = SpringBeanService.getBean(CoreserviceRemoteService.class);
            guacamoleConnectDto = coreserviceRemoteService.remoteServiceSimulatorVNCGuacamoleParam(simulatorId);
            if (guacamoleConnectDto == null) {
                //throw new BusinessException(ErrorCode.SIMULATOR_NON_EXISTENT, "获取远程模拟器不存在");
            }
            httpSession.setAttribute("guacamoleConnectDto", guacamoleConnectDto);
            httpSession.setAttribute("chk", "false");
        }
        GuacamoleConfiguration guacamoleConfiguration = new GuacamoleConfiguration();
        guacamoleConfiguration.setProtocol(guacamoleConnectDto.getProtocol());
        guacamoleConfiguration.setParameter("hostname", guacamoleConnectDto.getHostname());
        guacamoleConfiguration.setParameter("port", String.valueOf(guacamoleConnectDto.getPort()));
        if (StringUtils.isNotBlank(guacamoleConnectDto.getPassword())) {
            guacamoleConfiguration.setParameter("password", guacamoleConnectDto.getPassword());
        }
        guacamoleConfiguration.setParameter("width", "768");
        guacamoleConfiguration.setParameter("height", "1280");
        guacamoleConfiguration.setParameter("dpi", "320");

        GuacamoleClientInformation guacamoleClientInformation = new GuacamoleClientInformation();
        guacamoleClientInformation.setOptimalScreenWidth(768);
        guacamoleClientInformation.setOptimalScreenHeight(1280);
        guacamoleClientInformation.setOptimalResolution(320);

        String hostname = GuacamoleRemoteServiceProperties.getInstance().HOSTNAME;
        int port = GuacamoleRemoteServiceProperties.getInstance().PORT;
        GuacamoleSocket guacamoleSocket = new ConfiguredGuacamoleSocket(new InetGuacamoleSocket(hostname, port), guacamoleConfiguration, guacamoleClientInformation);
        GuacamoleTunnel guacamoleTunnel = new SimpleGuacamoleTunnel(guacamoleSocket);
        GuacamoleSession guacamoleSession = new GuacamoleSession(httpSession);
        guacamoleSession.attachTunnel(guacamoleTunnel);
        return guacamoleTunnel;
    }
}
