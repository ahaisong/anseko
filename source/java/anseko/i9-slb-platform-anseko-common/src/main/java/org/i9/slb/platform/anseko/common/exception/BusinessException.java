package org.i9.slb.platform.anseko.common.exception;

/**
 * 业务异常类
 *
 * @author R12
 * @date 2018.08.28
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -7687833436092301450L;

    private String message;

    public BusinessException(String message) {
        this.message = message;
    }

    public BusinessException(String message, Throwable cause) {
        super(cause);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
