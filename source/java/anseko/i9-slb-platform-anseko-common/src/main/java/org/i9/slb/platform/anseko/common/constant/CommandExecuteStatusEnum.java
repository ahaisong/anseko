package org.i9.slb.platform.anseko.common.constant;

import java.util.HashMap;

/**
 * 命令执行状态枚举
 *
 * @author tao.jiang03@ucarinc.com
 * @date 2019年3月11日 16:12:22
 */
public enum CommandExecuteStatusEnum {

    EXECUTING("执行中"), SUCCESS("成功"), ERROR("失败");

    private final String name;

    private CommandExecuteStatusEnum(String name) {
        this.name = name;
    }

    public static CommandExecuteStatusEnum valueOf(Integer status) {
        return types.get(status);
    }

    public String getName() {
        return name;
    }

    private static final HashMap<Integer, CommandExecuteStatusEnum> types = new HashMap<Integer, CommandExecuteStatusEnum>();

    static {
        for (CommandExecuteStatusEnum commandExecuteStatusEnum : values()) {
            types.put(commandExecuteStatusEnum.ordinal(), commandExecuteStatusEnum);
        }
    }
}
