package org.i9.slb.platform.anseko.hypervisors.param;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 虚拟化模拟器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:36
 */
public class SimulatorInfo {

    public static final int KB_MB_GB = 1024;

    public int getMemoryKB() {
        return getRamnum() * KB_MB_GB;
    }

    /**
     * 虚拟化模拟器构造函数
     *
     * @param name
     * @param cpunum
     * @param ramnum
     */
    public SimulatorInfo(String name, int cpunum, int ramnum, String instanceId) {
        this.uuid = UUID.randomUUID().toString();
        this.name = name;
        this.cpunum = cpunum;
        this.ramnum = ramnum;
        this.instanceId = instanceId;
    }

    /**
     * 虚拟器唯一编号
     */
    private String uuid;

    /**
     * 虚拟器名称
     */
    private String name;

    /**
     * CPU数量
     */
    private int cpunum;

    /**
     * 内存
     */
    private int ramnum;

    /**
     * 虚拟磁盘信息
     */
    private List<SimulatorDisk> simulatorDisks = new ArrayList<SimulatorDisk>();

    /**
     * 虚拟网卡信息
     */
    private List<SimulatorNetwork> simulatorNetworks = new ArrayList<SimulatorNetwork>();

    /**
     * 虚拟vnc信息
     */
    private SimulatorViewer simulatorViewer;
    /**
     * 实例编号
     */
    private String instanceId;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCpunum() {
        return cpunum;
    }

    public void setCpunum(int cpunum) {
        this.cpunum = cpunum;
    }

    public int getRamnum() {
        return ramnum;
    }

    public void setRamnum(int ramnum) {
        this.ramnum = ramnum;
    }

    public List<SimulatorDisk> getSimulatorDisks() {
        return simulatorDisks;
    }

    public void setSimulatorDisks(List<SimulatorDisk> simulatorDisks) {
        this.simulatorDisks = simulatorDisks;
    }

    public List<SimulatorNetwork> getSimulatorNetworks() {
        return simulatorNetworks;
    }

    public void setSimulatorNetworks(List<SimulatorNetwork> simulatorNetworks) {
        this.simulatorNetworks = simulatorNetworks;
    }

    public SimulatorViewer getSimulatorViewer() {
        return simulatorViewer;
    }

    public void setSimulatorViewer(SimulatorViewer simulatorViewer) {
        this.simulatorViewer = simulatorViewer;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
