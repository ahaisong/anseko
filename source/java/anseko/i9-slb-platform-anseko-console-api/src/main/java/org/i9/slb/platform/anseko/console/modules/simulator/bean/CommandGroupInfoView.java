package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.provider.dto.CommandGroupDto;

/**
 * 命令执行组
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/27 11:45
 */
public class CommandGroupInfoView implements java.io.Serializable {

    private static final long serialVersionUID = 6077603409898407486L;
    /**
     * 执行组编号
     */
    private String commandGroupId;
    /**
     * 组名称
     */
    private String name;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 起始日期
     */
    private String startDate;
    /**
     * 结束日期
     */
    private String endDate;
    /**
     * 完成结果
     */
    private Integer success;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public void copyProperty(CommandGroupDto commandGroupDto) {
        this.commandGroupId = commandGroupDto.getCommandGroupId();
        this.simulatorId = commandGroupDto.getSimulatorId();
        this.startDate = commandGroupDto.getStartDate();
        this.endDate = commandGroupDto.getEndDate();
        this.success = commandGroupDto.getSuccess();
        this.name = commandGroupDto.getName();
    }
}
