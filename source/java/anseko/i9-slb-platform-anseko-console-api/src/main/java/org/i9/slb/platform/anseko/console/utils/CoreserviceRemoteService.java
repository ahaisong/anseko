package org.i9.slb.platform.anseko.console.utils;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.SimpleCommandParamDto;
import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 调用coreservice远程服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:16
 */
@Service
public class CoreserviceRemoteService {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    /**
     * 调用远程服务发起一个调度任务
     *
     * @param simulatorId
     * @param nameLabel
     * @param groupCommandParamDto
     */
    public void remoteServiceLaunchCommandDispatch(String simulatorId, String nameLabel, GroupCommandParamDto groupCommandParamDto) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        int posIndex = 0;
        for (SimpleCommandParamDto simpleCommandParamDto : groupCommandParamDto.getCommands()) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandId(simpleCommandParamDto.getCommandId());
            commandExecuteDto.setCommandLine(simpleCommandParamDto.getCommandLine());
            commandExecuteDto.setPosIndex(posIndex++);
            commandExecuteDtos.add(commandExecuteDto);
        }
        CommandGroupDto commandGroupDto = new CommandGroupDto();
        commandGroupDto.setCommandGroupId(groupCommandParamDto.getGroupId());
        commandGroupDto.setSimulatorId(simulatorId);
        commandGroupDto.setCommandExecuteDtos(commandExecuteDtos);
        commandGroupDto.setName(nameLabel);

        DubboResult<?> dubboResult = dubboCommandRemoteService.launchCommandGroup(commandGroupDto);
        dubboResult.tryBusinessException();
    }

    /**
     * 调用远程服务发起一个调度任务
     *
     * @param simulatorId
     * @param nameLabel
     * @param shellCommandParamDto
     */
    public void remoteServiceLaunchCommandDispatch(String simulatorId, String nameLabel, ShellCommandParamDto shellCommandParamDto) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
        commandExecuteDto.setCommandId(shellCommandParamDto.getCommandId());
        commandExecuteDto.setCommandLine(shellCommandParamDto.getCommandLine());
        commandExecuteDto.setPosIndex(0);
        commandExecuteDtos.add(commandExecuteDto);
        CommandGroupDto commandGroupDto = new CommandGroupDto();
        commandGroupDto.setCommandExecuteDtos(commandExecuteDtos);
        commandGroupDto.setCommandGroupId(shellCommandParamDto.getCommandId());
        commandGroupDto.setSimulatorId(simulatorId);
        commandGroupDto.setName(nameLabel);
        dubboCommandRemoteService.launchCommandGroup(commandGroupDto);
    }

    /**
     * 调用远程服务发起一个调度任务
     *
     * @param simulatorId
     * @param shellCommandParamDtos
     */
    public void remoteServiceLaunchCommandDispatch(String simulatorId, String name, List<ShellCommandParamDto> shellCommandParamDtos) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        int posIndex = 0;
        for (ShellCommandParamDto shellCommandParamDto : shellCommandParamDtos) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandId(shellCommandParamDto.getCommandId());
            commandExecuteDto.setCommandLine(shellCommandParamDto.getCommandLine());
            commandExecuteDto.setPosIndex(posIndex++);
            commandExecuteDtos.add(commandExecuteDto);
        }
        CommandGroupDto commandGroupDto = new CommandGroupDto();
        commandGroupDto.setCommandGroupId(UUID.randomUUID().toString());
        commandGroupDto.setSimulatorId(simulatorId);
        commandGroupDto.setCommandExecuteDtos(commandExecuteDtos);
        commandGroupDto.setName(name);
        dubboCommandRemoteService.launchCommandGroup(commandGroupDto);
    }

    /**
     * 调用远程服务获取实例详情
     *
     * @param instanceId
     * @return
     */
    public InstanceDto remoteServiceGetInstanceDtoInfo(String instanceId) {
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(instanceId);
        dubboResult.tryBusinessException();
        InstanceDto instanceDto = dubboResult.getRe();
        return instanceDto;
    }
}
