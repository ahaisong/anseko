package org.i9.slb.platform.anseko.console.modules.user.bean;

/**
 * 用户更新表单
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/12 16:09
 */
public class UserUpdateForm implements java.io.Serializable {

    private static final long serialVersionUID = -240959046039201011L;
    /**
     * 用户编号
     */
    private String id;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像
     */
    private String portraitUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPortraitUrl() {
        return portraitUrl;
    }

    public void setPortraitUrl(String portraitUrl) {
        this.portraitUrl = portraitUrl;
    }
}
