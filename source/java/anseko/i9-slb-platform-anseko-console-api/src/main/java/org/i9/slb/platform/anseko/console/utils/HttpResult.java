package org.i9.slb.platform.anseko.console.utils;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 操作返回值
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 14:44
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HttpResult<T> implements java.io.Serializable {

    private static final long serialVersionUID = -7530145176229215330L;

    /**
     * 返回结果码
     */
    private Integer result;
    /**
     * 返回消息
     */
    private String message;
    /**
     * 返回数据
     */
    private T re;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getRe() {
        return re;
    }

    public void setRe(T re) {
        this.re = re;
    }
}
