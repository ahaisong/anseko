package org.i9.slb.platform.anseko.console.modules.instance.bean;

import org.i9.slb.platform.anseko.common.types.InstanceStatusEnum;
import org.i9.slb.platform.anseko.common.types.VirtualEnum;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;

/**
 * 实列信息视图
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/21 16:12
 */
public class InstanceInfoView implements java.io.Serializable {

    private static final long serialVersionUID = -8577385373956139661L;

    public VirtualEnum getVirtualEnum() {
        VirtualEnum virtualEnum = VirtualEnum.valueOf(this.virtualType);
        return virtualEnum;
    }

    public InstanceStatusEnum getInstanceStatusEnum() {
        InstanceStatusEnum instanceStatusEnum = InstanceStatusEnum.valueOf(this.status);
        return instanceStatusEnum;
    }

    public String getVirtualDesc() {
        return getVirtualEnum().name();
    }

    public String getStatusDesc() {
        return getInstanceStatusEnum().getName();
    }

    /**
     * 实例编号
     */
    private String id;
    /**
     * 实例名称
     */
    private String instanceName;
    /**
     * 虚拟化类型
     *
     * @see org.i9.slb.platform.anseko.common.types.VirtualEnum
     */
    private Integer virtualType;
    /**
     * 远程地址
     */
    private String remoteAddress;
    /**
     * 创建日期
     */
    private String createDate;
    /**
     * 更新日期
     */
    private String updateDate;
    /**
     * 状态（1 : 启用，0 : 禁用）
     *
     * @see org.i9.slb.platform.anseko.common.types.InstanceStatusEnum
     */
    private Integer status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public Integer getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(Integer virtualType) {
        this.virtualType = virtualType;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void copyProperty(InstanceDto instanceDto) {
        this.id = instanceDto.getId();
        this.instanceName = instanceDto.getInstanceName();
        this.remoteAddress = instanceDto.getRemoteAddress();
        this.virtualType = instanceDto.getVirtualType();
        this.createDate = instanceDto.getCreateDate();
        this.updateDate = instanceDto.getUpdateDate();
        this.status = instanceDto.getStatus();
    }
}
