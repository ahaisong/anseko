package org.i9.slb.platform.anseko.console.webmvc.interceptor;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.console.modules.user.service.UserTokenRedisService;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用户登录拦截器
 *
 * @author r12
 * @date 2019年2月12日 11:12:22
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserTokenRedisService userTokenRedisService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        final String userToken = httpServletRequest.getHeader("userToken");
        if (StringUtils.isBlank(userToken)) {
            outRequestForJson(httpServletResponse);
            return false;
        }
        String data = this.userTokenRedisService.getUserTokenValue(userToken);
        if (StringUtils.isBlank(data)) {
            outRequestForJson(httpServletResponse);
            return false;
        }
        this.userTokenRedisService.refreshUserTokenExpire(userToken);
        return super.preHandle(httpServletRequest, httpServletResponse, o);
    }

    private void outRequestForJson(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.setContentType("application/json; charset=UTF-8");
        HttpResult<?> httpResult = HttpResponse.error(ErrorCode.NON_LOGIN, new BusinessException("用户未登录"));
        httpServletResponse.getWriter().print(JSONObject.toJSONString(httpResult));
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }
}
