package org.i9.slb.platform.anseko.console.modules.common.controller;

import org.i9.slb.platform.anseko.console.modules.common.bean.UserLoginView;
import org.i9.slb.platform.anseko.console.modules.user.service.UserService;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService userService;

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/userLogin.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<UserLoginView> login(@RequestParam("username") String username, @RequestParam("password") String password) {
        LOGGER.info("用户登录, username : {}, password : {}", username, password);
        UserLoginView userLoginView = this.userService.userLogin(username, password);
        return HttpResponse.ok(userLoginView);
    }

    @RequestMapping(value = "/userLogout.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> userLogout(@RequestHeader("userToken") String userToken) {
        this.userService.userLogout(userToken);
        return HttpResponse.ok();
    }
}
