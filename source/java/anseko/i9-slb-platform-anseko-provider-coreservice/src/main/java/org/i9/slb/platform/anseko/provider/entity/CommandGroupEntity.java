package org.i9.slb.platform.anseko.provider.entity;

/**
 * 命令调度器实体类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:52
 */
public class CommandGroupEntity implements java.io.Serializable {

    private static final long serialVersionUID = -5531793954783178004L;
    /**
     * 编号
     */
    private String id;

    private String name;
    /**
     * 命令组编号
     */
    private String commandGroupId;
    /**
     * 模拟器编号
     */
    private String simulatorId;

    /**
     * 起始日期
     */
    private String startDate;
    /**
     * 结束日期
     */
    private String endDate;
    /**
     * 是否成功
     */
    private Integer success;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
