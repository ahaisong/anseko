package org.i9.slb.platform.anseko.provider.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.i9.slb.platform.anseko.provider.entity.UserEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.UserQuery;

import java.util.List;

/**
 * 用户信息管理dao
 *
 * @author r12
 * @date 2019年2月13日 18:11:17
 */
public interface UserRepository {

    /**
     * 查询所有用户信息
     *
     * @param userQuery
     * @return
     */
    List<UserEntity> getUserEntityList(UserQuery userQuery);

    /**
     * 通过用户名查询用户信息
     *
     * @param username
     * @return
     */
    UserEntity getUserEntityByUsername(@Param("username") String username);

    /**
     * 通过id查询用户信息
     *
     * @param id
     * @return
     */
    @Select("SELECT id, username, password, nickname, portraitUrl, createDate, updateDate FROM tb_user WHERE id = #{id}")
    UserEntity getUserEntityById(@Param("id") String id);

    /**
     * 保存用户信息
     *
     * @param userEntity
     */
    void insertUserEntity(UserEntity userEntity);

    /**
     * 更新用户信息
     *
     * @param userEntity
     */
    void updateUserEntity(UserEntity userEntity);

    /**
     * 删除用户信息
     *
     * @param id
     */
    @Delete("DELETE FROM tb_user WHERE id = #{id}")
    void deleteUserById(@Param("id") String id);

    /**
     * 统计用户名数量
     *
     * @param username
     * @return
     */
    @Select("SELECT COUNT(1) FROM tb_user WHERE username = #{username}")
    int countUserEntityByUsernameNumber(@Param("username") String username);

    @Update("UPDATE tb_user SET password = MD5(#{password}) WHERE id = #{userId}")
    void updateUserEntityPassword(@Param("userId") String userId, @Param("password") String password);
}
