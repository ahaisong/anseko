package org.i9.slb.platform.anseko.provider.repository.querybean;

public class BasePageQuery implements java.io.Serializable {

    private static final long serialVersionUID = 740435092083232024L;
    /**
     * 起始page
     */
    private int startPage;
    /**
     * 当页总数
     */
    private int pageSize;

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
