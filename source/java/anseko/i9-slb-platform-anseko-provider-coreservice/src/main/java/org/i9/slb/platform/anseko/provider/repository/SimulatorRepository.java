package org.i9.slb.platform.anseko.provider.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.i9.slb.platform.anseko.provider.entity.SimulatorEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.SimulatorQuery;

import java.util.List;

/**
 * 模拟器dao
 *
 * @author R12
 * @date 2018年9月4日 10:49:40
 */
public interface SimulatorRepository {

    /**
     * 获取模拟器列表
     *
     * @return
     */
    List<SimulatorEntity> getSimulatorEntityList();

    /**
     * 获取模拟器信息
     *
     * @param id
     * @return
     */
    SimulatorEntity getSimulatorEntityById(@Param("id") String id);

    /**
     * 保存模拟器信息
     *
     * @param simulatorEntity
     */
    void insertSimulatorEntity(SimulatorEntity simulatorEntity);

    /**
     * 获取当前实例中最大vncport
     *
     * @param instanceId
     * @return
     */
    @Select("SELECT MAX(vncPort) FROM tb_simulator WHERE instanceId = #{instanceId}")
    Integer getSimulatorEntityMaxVncPort(@Param("instanceId") String instanceId);

    /**
     * 删除模拟器
     *
     * @param id
     */
    @Delete("DELETE FROM tb_simulator WHERE id = #{id}")
    void deleteSimulatorEntity(@Param("id") String id);

    /**
     * 更新模拟器状态
     *
     * @param id
     * @param powerState
     */
    @Update("UPDATE tb_simulator SET powerStatus = #{powerState} WHERE id = #{id}")
    void updateSimulatorEntityPowerState(@Param("id") String id, @Param("powerState") int powerState);

    /**
     * 更新模拟器信息
     *
     * @param simulatorEntity
     */
    void updateSimulatorEntity(SimulatorEntity simulatorEntity);

    /**
     * 查询模拟器列表分页
     *
     * @param simulatorQuery
     * @return
     */
    List<SimulatorEntity> getSimulatorEntityListPage(SimulatorQuery simulatorQuery);

    /**
     * 统计模拟器名称数量
     *
     * @param simulatorName
     * @return
     */
    @Select("SELECT COUNT(1) FROM tb_simulator WHERE simulatorName = #{simulatorName}")
    int getCountSimulatorNameNumber(String simulatorName);

    @Update("UPDATE tb_simulator SET status = #{status} WHERE id = #{simulatorId}")
    void updateSimulatorEntityStatus(@Param("simulatorId") String simulatorId, @Param("status") int status);
}
