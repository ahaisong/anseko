package org.i9.slb.platform.anseko.provider.repository;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.i9.slb.platform.anseko.provider.entity.CommandGroupEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.CommandGroupQuery;

import java.util.List;

/**
 * 命令调度dao
 *
 * @author R12
 * @date 2018年9月4日 10:50:09
 */
public interface CommandGroupRepository {

    /**
     * 保存命令调度信息
     *
     * @param commandGroupEntity
     */
    void insertCommandGroupEntity(CommandGroupEntity commandGroupEntity);

    /**
     * 更新命令调度状态
     *
     * @param commandGroupId
     * @param success
     */
    @Update("UPDATE tb_command_group SET endDate = NOW(), success = #{success} WHERE commandGroupId = #{commandGroupId}")
    void updateCommandGroupEndTime(@Param("commandGroupId") String commandGroupId, @Param("success") int success);

    /**
     * 获取模拟器命令调度列表
     *
     * @param simulatorId
     * @return
     */
    List<CommandGroupEntity> getCommandGroupEntityList(@Param("simulatorId") String simulatorId);

    /**
     * 获取命令调度信息
     *
     * @param commandGroupId
     * @return
     */
    CommandGroupEntity getCommandGroupByGroupId(@Param("commandGroupId") String commandGroupId);

    /**
     * 获取模拟器命令组列表分页
     *
     * @param commandGroupQuery
     * @return
     */
    List<CommandGroupEntity> getCommandGroupEntityListPage(CommandGroupQuery commandGroupQuery);
}
