package org.i9.slb.platform.anseko.provider.repository;

import org.i9.slb.platform.anseko.provider.entity.SimulatorDisplayEntity;

public interface SimulatorDisplayRepository {

    void insertSimulatorDisplay(SimulatorDisplayEntity simulatorDisplayEntity);

    SimulatorDisplayEntity getSimulatorDisplayEntity(String simulatorId);

    Integer queryNextVNCPort();
}
