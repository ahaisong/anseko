package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.dto.SimulatorFirewallDto;
import org.i9.slb.platform.anseko.provider.entity.SimulatorFirewallEntity;
import org.i9.slb.platform.anseko.provider.repository.SimulatorFirewallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 模拟器端口映射服务类
 *
 * @author r12
 */
@Service
public class SimulatorFirewallService {

    @Autowired
    private SimulatorFirewallRepository simulatorFirewallRepository;

    /**
     * 创建模拟器端口映射
     *
     * @param simulatorId
     * @param simulatorFirewallDtos
     */
    @Transactional(rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
    public void createSimulatorFirewall(String simulatorId, List<SimulatorFirewallDto> simulatorFirewallDtos) {
        for (SimulatorFirewallDto simulatorFirewallDto : simulatorFirewallDtos) {
            SimulatorFirewallEntity simulatorFirewallEntity = new SimulatorFirewallEntity();
            simulatorFirewallEntity.setId(UUIDUtil.generateUUID());
            simulatorFirewallEntity.setSimulatorId(simulatorId);
            simulatorFirewallEntity.setSport(simulatorFirewallDto.getSport());
            simulatorFirewallEntity.setDport(simulatorFirewallDto.getDport());
            simulatorFirewallEntity.setCreateDate(new Date());
            simulatorFirewallEntity.setUpdateDate(new Date());
            simulatorFirewallEntity.setNameLabel(simulatorFirewallDto.getNameLabel());
            this.simulatorFirewallRepository.insertSimulatorFirewall(simulatorFirewallEntity);
        }
    }

    /**
     * 获取模拟器端口映射
     *
     * @param simulatorId
     * @return
     */
    public List<SimulatorFirewallEntity> getSimulatorFirewallEntityList(String simulatorId) {
        List<SimulatorFirewallEntity> list = this.simulatorFirewallRepository.getSimulatorFirewallEntityList(simulatorId);
        return list;
    }

    /**
     * 复制属性
     *
     * @param simulatorFirewallEntity
     * @param simulatorFirewallDto
     */
    public void copyProperty(SimulatorFirewallEntity simulatorFirewallEntity, SimulatorFirewallDto simulatorFirewallDto) {
        simulatorFirewallDto.setId(simulatorFirewallEntity.getId());
        simulatorFirewallDto.setSimulatorId(simulatorFirewallEntity.getSimulatorId());
        simulatorFirewallDto.setCreateDate(simulatorFirewallEntity.getCreateDate());
        simulatorFirewallDto.setUpdateDate(simulatorFirewallEntity.getUpdateDate());
        simulatorFirewallDto.setSport(simulatorFirewallEntity.getSport());
        simulatorFirewallDto.setDport(simulatorFirewallEntity.getDport());
        simulatorFirewallDto.setNameLabel(simulatorFirewallEntity.getNameLabel());
    }

    /**
     * 创建模拟器端口映射
     *
     * @param simulatorFirewallDto
     */
    public void createSimulatorFirewall(SimulatorFirewallDto simulatorFirewallDto) {
        SimulatorFirewallEntity simulatorFirewallEntity = new SimulatorFirewallEntity();
        simulatorFirewallEntity.setId(UUIDUtil.generateUUID());
        simulatorFirewallEntity.setSimulatorId(simulatorFirewallDto.getSimulatorId());
        simulatorFirewallEntity.setSport(simulatorFirewallDto.getSport());
        simulatorFirewallEntity.setDport(simulatorFirewallDto.getDport());
        simulatorFirewallEntity.setCreateDate(new Date());
        simulatorFirewallEntity.setUpdateDate(new Date());
        simulatorFirewallEntity.setNameLabel(simulatorFirewallDto.getNameLabel());
        this.simulatorFirewallRepository.insertSimulatorFirewall(simulatorFirewallEntity);
    }
}
