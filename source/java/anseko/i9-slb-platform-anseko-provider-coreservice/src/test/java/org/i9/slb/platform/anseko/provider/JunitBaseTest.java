package org.i9.slb.platform.anseko.provider;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dubbo-provider.xml"})
public class JunitBaseTest {

    private static final List<String> TABLE_NAME_LIST = new ArrayList<String>();

    static {
        TABLE_NAME_LIST.add("tb_instance");
        TABLE_NAME_LIST.add("tb_simulator");
        TABLE_NAME_LIST.add("tb_command_group");
        TABLE_NAME_LIST.add("tb_command_execute");
        TABLE_NAME_LIST.add("tb_user");
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
//        for (String tableName : TABLE_NAME_LIST) {
//            this.jdbcTemplate.update("DELETE FROM " + tableName);
//        }
    }
}
