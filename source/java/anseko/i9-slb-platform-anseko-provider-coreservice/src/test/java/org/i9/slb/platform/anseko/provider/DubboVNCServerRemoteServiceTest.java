package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.service.dubbo.DubboVNCServerRemoteService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DubboVNCServerRemoteServiceTest extends JunitBaseTest {

    @Autowired
    private DubboVNCServerRemoteService dubboVNCServerRemoteService;

    @Test
    public void testSimulatorVNCGuacamoleParam() {
        dubboVNCServerRemoteService.simulatorVNCGuacamoleParam("43fa232a-2aa9-4b65-99d4-9d53ed32e48a");
    }
}
