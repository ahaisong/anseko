package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

public class RRE extends Rect {
    public static class SubRect {

        public int pixel;
        public int x;
        public int y;
        public int w;
        public int h;

        public SubRect() {
        }
    }

    public PixelFormat pixelFormat;
    public int bgpixel;
    public SubRect subrects[];

    public RRE(int ai[], PixelFormat pixelformat, int i, int j, int k, int l, int i1,
               int j1, int k1) {
        super(l, i1, j1, k1);
        pixelFormat = pixelformat;
        ai = copyPixels(ai, k, l - i, i1 - j, j1, k1);
        Vector vector = new Vector();
        int j3 = 0;
        int j4 = 0;
        bgpixel = getBackground(ai, j1, 0, 0, j1, k1);
        for (int j2 = 0; j2 < k1; j2++) {
            int k5 = j2 * j1;
            label0:
            for (int i2 = 0; i2 < j1; i2++) {
                if (ai[k5 + i2] == bgpixel)
                    continue;
                int l1 = ai[k5 + i2];
                int k3 = j2 - 1;
                boolean flag = true;
                int i3;
                for (i3 = j2; i3 < k1; i3++) {
                    int j5 = i3 * j1;
                    if (ai[j5 + i2] != l1)
                        break;
                    int k2;
                    for (k2 = i2; k2 < j1 && ai[j5 + k2] == l1; k2++) ;
                    k2--;
                    if (i3 == j2)
                        j4 = j3 = k2;
                    if (k2 < j4)
                        j4 = k2;
                    if (flag && k2 >= j3)
                        k3++;
                    else
                        flag = false;
                }

                int k4 = i3 - 1;
                int l3 = (j3 - i2) + 1;
                int i4 = (k3 - j2) + 1;
                int l4 = (j4 - i2) + 1;
                int i5 = (k4 - j2) + 1;
                SubRect subrect = new SubRect();
                vector.addElement(subrect);
                subrect.pixel = l1;
                subrect.x = i2;
                subrect.y = j2;
                if (l3 * i4 > l4 * i5) {
                    subrect.w = l3;
                    subrect.h = i4;
                } else {
                    subrect.w = l4;
                    subrect.h = i5;
                }
                i3 = subrect.y;
                do {
                    if (i3 >= subrect.y + subrect.h)
                        continue label0;
                    for (int l2 = subrect.x; l2 < subrect.x + subrect.w; l2++)
                        ai[i3 * j1 + l2] = bgpixel;

                    i3++;
                } while (true);
            }

        }

        subrects = new SubRect[vector.size()];
        vector.toArray((Object[]) subrects);
    }

    public RRE(int i, int j, int k, int l, PixelFormat pixelformat, int i1, SubRect asubrect[]) {
        super(i, j, k, l);
        pixelFormat = pixelformat;
        bgpixel = i1;
        subrects = asubrect;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        super.writeData(dataoutput);
        dataoutput.writeInt(2);
        dataoutput.writeInt(subrects.length);
        writePixel(dataoutput, pixelFormat, bgpixel);
        for (int i = 0; i < subrects.length; i++) {
            writePixel(dataoutput, pixelFormat, subrects[i].pixel);
            dataoutput.writeShort(subrects[i].x);
            dataoutput.writeShort(subrects[i].y);
            dataoutput.writeShort(subrects[i].w);
            dataoutput.writeShort(subrects[i].h);
        }

    }

    public Object clone()
            throws CloneNotSupportedException {
        SubRect asubrect[] = new SubRect[subrects.length];
        for (int i = 0; i < subrects.length; i++) {
            asubrect[i] = new SubRect();
            asubrect[i].pixel = subrects[i].pixel;
            asubrect[i].x = subrects[i].x;
            asubrect[i].y = subrects[i].y;
            asubrect[i].w = subrects[i].w;
            asubrect[i].h = subrects[i].h;
        }

        return new RRE(x, y, w, h, pixelFormat, bgpixel, asubrect);
    }
}
