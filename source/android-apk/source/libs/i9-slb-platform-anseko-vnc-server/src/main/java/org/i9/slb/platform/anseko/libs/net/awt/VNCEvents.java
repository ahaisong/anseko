package org.i9.slb.platform.anseko.libs.net.awt;

import org.i9.slb.platform.anseko.libs.net.rfb.keysym;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBClient;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBClients;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBSocket;

import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.HashMap;

public class VNCEvents {
    private static class State {

        public int keyModifiers;
        public int mouseModifiers;
        public Component oldComponent;
        public int oldX;
        public int oldY;
        public boolean dragging;
        public long lastMouseClickTime[];

        private State() {
            keyModifiers = 0;
            mouseModifiers = 0;
            oldComponent = null;
            dragging = false;
            lastMouseClickTime = new long[5];
        }

    }

    static Object log;
    static Method logmethod;
    private Window container;
    private RFBClients clients;
    private static HashMap eventMap = new HashMap();

    private void logDebug(String s) {
        try {
            if (log == null) {
                log = System.err;
                logmethod = (PrintStream.class).getMethod("println", new Class[]{
                        String.class
                });
            }
            logmethod.invoke(log, new Object[]{
                    s
            });
        } catch (Exception exception) {
            exception.printStackTrace();
            System.err.println(s);
        }
    }

    public VNCEvents(Window window, RFBClients rfbclients) {
        container = window;
        clients = rfbclients;
    }

    public void translateKeyEvent(RFBClient rfbclient, boolean flag, int i) {
        State state = getState(rfbclient);
        int j = keysym.toMask(i);
        if (j != 0) {
            if (flag)
                state.keyModifiers |= j;
            else
                state.keyModifiers &= ~j;
            return;
        }
        char c = '\0';
        int k = keysym.toVK(i);
        if (k == 0)
            c = (char) i;
        if (flag) {
            fireKeyEvent(rfbclient, 401, k, c, state.keyModifiers, null);
        } else {
            fireKeyEvent(rfbclient, 402, k, c, state.keyModifiers, null);
            if (k == 0)
                fireKeyEvent(rfbclient, 400, k, c, state.keyModifiers, null);
        }
    }

    public void translatePointerEvent(RFBClient rfbclient, int i, int j, int k) {
        State state = getState(rfbclient);
        int l = 0;
        boolean flag = false;
        byte byte0 = -1;
        if ((i & 1) != 0) {
            byte0 = 0;
            l |= 0x10;
            flag = (l & 0x10) > 0;
        }
        if ((i & 2) != 0) {
            byte0 = 1;
            l |= 8;
            flag = (l & 8) > 0;
        }
        if ((i & 4) != 0) {
            byte0 = 2;
            l |= 4;
            flag = (l & 4) > 0;
        }
        Insets insets = container.getInsets();
        j += insets.left;
        k += insets.top;
        Component component;
        if (state.dragging)
            component = state.oldComponent;
        else
            component = container.findComponentAt(j, k);
        state.dragging = false;
        if (l == state.mouseModifiers) {
            if (l == 0) {
                fireMouseEvent(rfbclient, container, 503, j, k, 0, state.keyModifiers | state.mouseModifiers);
            } else {
                state.dragging = true;
                fireMouseEvent(rfbclient, container, 506, j, k, 0, state.keyModifiers | state.mouseModifiers);
            }
        } else {
            if (flag) {
                byte byte1 = 1;
                long l1 = System.currentTimeMillis() - state.lastMouseClickTime[byte0];
                if (l1 < 1000L)
                    byte1 = 2;
                state.mouseModifiers = l;
                state.lastMouseClickTime[byte0] = System.currentTimeMillis();
                fireMouseEvent(rfbclient, container, 501, j, k, byte1, state.keyModifiers | state.mouseModifiers);
                state.lastMouseClickTime[byte0] = System.currentTimeMillis();
                fireMouseEvent(rfbclient, container, 500, j, k, byte1, state.keyModifiers | state.mouseModifiers);
            } else {
                fireMouseEvent(rfbclient, container, 502, j, k, 0, state.keyModifiers | state.mouseModifiers);
            }
            state.mouseModifiers = l;
            Object obj = getFocusComponent(container);
            if (obj != container) {
                if (obj != null)
                    fireEvent(rfbclient, new FocusEvent(((Component) (obj)), 1005));
                if (component != null) {
                    fireEvent(rfbclient, new FocusEvent(component.getParent(), 1004));
                    fireEvent(rfbclient, new FocusEvent(component, 1004));
                }
                obj = container;
            }
        }
        if (component != state.oldComponent) {
            if (state.oldComponent != null)
                fireMouseEvent(rfbclient, container, 505, state.oldX, state.oldY, 0, state.keyModifiers | state.mouseModifiers);
            fireMouseEvent(rfbclient, container, 504, j, k, 0, state.keyModifiers | state.mouseModifiers);
            state.oldComponent = component;
            state.oldX = j;
            state.oldY = k;
        }
    }

    public static HashMap getEventMap() {
        return eventMap;
    }

    private State getState(RFBClient rfbclient) {
        State state = (State) clients.getProperty(rfbclient, "events");
        if (state == null) {
            state = new State();
            clients.setProperty(rfbclient, "events", state);
        }
        return state;
    }

    private void fireEvent(RFBClient rfbclient, ComponentEvent componentevent) {
        if (componentevent == null) {
            System.err.println("VNCEvents.fireEvent(event==null)");
            return;
        } else {
            ((Component) componentevent.getSource()).enableInputMethods(false);
            getEventMap().put(componentevent, ((RFBSocket) rfbclient).getInetAddress().getHostName());
            ((Component) componentevent.getSource()).getToolkit().getSystemEventQueue().postEvent(componentevent);
            return;
        }
    }

    private void fireKeyEvent(RFBClient rfbclient, int i, int j, char c, int k, Component component) {
        if (j == 8) {
            j = 127;
            c = '\177';
        }
        KeyEvent keyevent = new KeyEvent(container, i, System.currentTimeMillis(), k, j, c);
        logDebug("fireKeyEvent(id=" + i + ", vk=" + j + " character=" + c + " getKeyText=" + keyevent.getKeyText(keyevent.getKeyCode()) + " container=" + container.getClass().getName() + "focused compunent=" + (component != null ? component.getClass().getName() : "null"));
        fireEvent(rfbclient, keyevent);
    }

    private void fireMouseEvent(RFBClient rfbclient, Component component, int i, int j, int k, int l, int i1) {
        fireEvent(rfbclient, new MouseEvent(component, i, System.currentTimeMillis(), i1, j, k, l, false));
    }

    private Point getLocation(Component component) {
        Point point = component.getLocation();
        if (component == container)
            return point;
        Container container1 = component.getParent();
        if (container1 != null) {
            Point point1 = container1.getLocationOnScreen();
            return new Point(point.x - point1.x, point.y - point1.y);
        } else {
            return point;
        }
    }

    private Component getFocusComponent(Component component) {
        if (component.hasFocus())
            return component;
        if (component instanceof Container) {
            Component acomponent[] = ((Container) component).getComponents();
            Object obj = null;
            for (int i = 0; i < acomponent.length; i++) {
                Component component1 = getFocusComponent(acomponent[i]);
                if (component1 != null)
                    return component1;
            }

        }
        return null;
    }

}
