package org.i9.slb.platform.anseko.libs.net.client;

import org.i9.slb.platform.anseko.libs.net.rfb.Colour;
import org.i9.slb.platform.anseko.libs.net.rfb.KeyCode;
import org.i9.slb.platform.anseko.libs.net.rfb.PixelFormat;
import org.i9.slb.platform.anseko.libs.net.rfb.Rect;
import org.i9.slb.platform.anseko.libs.net.rfb.keysym;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBClient;
import org.i9.slb.platform.anseko.libs.net.rfb.server.RFBServer;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class VNCRobot extends Component
        implements RFBServer, Runnable {
    static String OS = System.getProperty("os.name").toLowerCase();
    static boolean isWindows = (OS.indexOf("windows") >= 0) || (OS.indexOf("NT") >= 0);
    boolean shift = false;
    static final int CELLS = 4;
    Rectangle[] rects = new Rectangle[16];
    BufferedImage[] oldImages = new BufferedImage[16];
    int[] rectRatos = new int[16];
    static int clientNo = 0;
    Map clients = new HashMap();
    private PixelFormat defaultPixel = null;
    private int times = 0;
    private String displayName;
    private GraphicsDevice device;
    private Robot robot;
    private int mouseModifiers = 0;
    private boolean stop;
    int dw;
    int dh;

    public VNCRobot(int paramInt, String paramString) {
        this.displayName = paramString;
        this.device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        try {
            this.robot = new Robot();
            new Thread(this).start();
        } catch (AWTException localAWTException) {
            localAWTException.printStackTrace();
        }
    }

    public void addClient(RFBClient paramRFBClient) {
        if (!this.clients.containsKey(paramRFBClient)) {
            RobotClient localRobotClient = new RobotClient();
            localRobotClient.no = (clientNo++);
            this.clients.put(paramRFBClient, localRobotClient);
        }
    }

    public void removeClient(RFBClient paramRFBClient) {
        if ((paramRFBClient != null) && (this.clients.containsKey(paramRFBClient))) {
            System.out.println("Closed client:" + paramRFBClient);
            this.clients.remove(paramRFBClient);
            try {
                paramRFBClient.close();
            } catch (Exception localException) {
            }
        }
    }

    public String getDesktopName(RFBClient paramRFBClient) {
        RobotClient localRobotClient = (RobotClient) this.clients.get(paramRFBClient);
        if (localRobotClient == null) {
            addClient(paramRFBClient);
            localRobotClient = (RobotClient) this.clients.get(paramRFBClient);
        }
        return this.displayName + "_" + localRobotClient.no;
    }

    public int getFrameBufferWidth(RFBClient paramRFBClient) {
        return this.device.getDefaultConfiguration().getBounds().width;
    }

    public int getFrameBufferHeight(RFBClient paramRFBClient) {
        return this.device.getDefaultConfiguration().getBounds().height;
    }

    public PixelFormat getPreferredPixelFormat(RFBClient paramRFBClient) {
        return PixelFormat.RGB888;
    }

    public boolean allowShared() {
        return true;
    }

    public void setClientProtocolVersionMsg(RFBClient paramRFBClient, String paramString)
            throws IOException {
        System.out.println("version " + paramString);
    }

    public void setShared(RFBClient paramRFBClient, boolean paramBoolean)
            throws IOException {
        System.out.println("share " + paramBoolean);
    }

    public void setPixelFormat(RFBClient paramRFBClient, PixelFormat paramPixelFormat)
            throws IOException {
        if (paramPixelFormat != null)
            this.defaultPixel = paramPixelFormat;
        paramPixelFormat.setColorModel(Toolkit.getDefaultToolkit().getColorModel());
        System.out.println("pixelFormat");
        paramPixelFormat.print(System.out);
    }

    public void setEncodings(RFBClient paramRFBClient, int[] paramArrayOfInt)
            throws IOException {
        System.out.print("encodings");
        for (int i = 0; i < paramArrayOfInt.length; i++) {
            System.out.print(paramArrayOfInt[i]);
            System.out.print(",");
        }
        System.out.println();
    }

    public void fixColourMapEntries(RFBClient paramRFBClient, int paramInt, Colour[] paramArrayOfColour)
            throws IOException {
        RobotClient localRobotClient = (RobotClient) this.clients.get(paramRFBClient);
        if (localRobotClient == null) {
            addClient(paramRFBClient);
            localRobotClient = (RobotClient) this.clients.get(paramRFBClient);
        }
        localRobotClient.colourMap = paramArrayOfColour;
        System.out.println("fixColourMapEntries");
    }

    public void frameBufferUpdateRequest(RFBClient paramRFBClient, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
            throws IOException {
        int i = getFrameBufferWidth(paramRFBClient);
        int j = getFrameBufferHeight(paramRFBClient);
        if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt3 <= 0) || (paramInt4 <= 0)) {
            System.out.println("Neg:" + paramInt1 + ":" + paramInt2 + ":" + paramInt3 + ":" + paramInt4);
            return;
        }
        if (paramInt1 + paramInt3 > i) {
            System.out.println("Too wide");
            return;
        }
        if (paramInt2 + paramInt4 > j) {
            System.out.println("Too high");
            return;
        }
        PixelFormat localPixelFormat = paramRFBClient.getPixelFormat();
        if (localPixelFormat == null)
            localPixelFormat = this.defaultPixel;
        if (localPixelFormat == null)
            return;
        Rect[] arrayOfRect = null;
        try {
            BufferedImage localBufferedImage = this.robot.createScreenCapture(new Rectangle(paramInt1, paramInt2, paramInt3, paramInt4));
            Rect localRect = Rect.encode(paramRFBClient.getPreferredEncoding(), localPixelFormat, localBufferedImage, paramInt1, paramInt2, paramInt3, paramInt4);
            arrayOfRect = new Rect[]{localRect};
            System.out.println("Client Update:" + paramInt1 + ":" + paramInt2 + ":" + paramInt3 + ":" + paramInt4);
            this.times = -1;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        if (arrayOfRect != null)
            paramRFBClient.writeFrameBufferUpdate(arrayOfRect);
    }

    public void keyEvent(RFBClient paramRFBClient, boolean paramBoolean, int paramInt)
            throws IOException {
        KeyCode localKeyCode = keysym.toVKCode(paramInt);
        if (localKeyCode != null)
            try {
                if (paramBoolean) {
                    if (localKeyCode.isShift) ;
                    this.robot.keyPress(localKeyCode.key);
                    if (localKeyCode.key == 16)
                        this.shift = true;
                } else {
                    this.robot.keyRelease(localKeyCode.key);
                    if (localKeyCode.key == 16)
                        this.shift = false;
                    if (!localKeyCode.isShift) ;
                }
            } catch (Exception localException1) {
                System.out.println(localException1.getMessage());
            }
        else if ((isWindows) && (!paramBoolean))
            try {
                if (this.shift)
                    this.robot.keyRelease(16);
                this.robot.keyPress(18);
                String str1 = String.valueOf(paramInt);
                for (int i = 0; i < str1.length(); i++) {
                    String str2 = str1.substring(i, i + 1);
                    int j = Integer.parseInt(str2);
                    this.robot.keyPress(96 + j);
                    this.robot.keyRelease(96 + j);
                }
                this.robot.keyRelease(18);
            } catch (Exception localException2) {
                localException2.printStackTrace();
            } finally {
                try {
                    this.robot.keyRelease(18);
                } catch (Exception localException5) {
                    localException5.printStackTrace();
                }
            }
    }

    public void pointerEvent(RFBClient paramRFBClient, int paramInt1, int paramInt2, int paramInt3)
            throws IOException {
        int i = 0;
        if ((paramInt1 & 0x1) != 0)
            i |= 16;
        if ((paramInt1 & 0x2) != 0)
            i |= 8;
        if ((paramInt1 & 0x4) != 0)
            i |= 4;
        try {
            if (i != this.mouseModifiers) {
                if (this.mouseModifiers == 0) {
                    this.robot.mouseMove(paramInt2, paramInt3);
                    this.robot.mousePress(i);
                } else {
                    this.robot.mouseRelease(this.mouseModifiers);
                }
                this.mouseModifiers = i;
            } else {
                this.robot.mouseMove(paramInt2, paramInt3);
            }
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    public void clientCutText(RFBClient paramRFBClient, String paramString)
            throws IOException {
    }

    public boolean isUpdateAvailable(RFBClient paramRFBClient) {
        return false;
    }

    private Rectangle alignRectangle(Rectangle paramRectangle) {
        int i = paramRectangle.x % 16;
        if (i != 0)
            paramRectangle.x -= i;
        i = paramRectangle.y % 16;
        if (i != 0)
            paramRectangle.y -= i;
        i = paramRectangle.width % 16;
        if (i != 0) {
            paramRectangle.width = (paramRectangle.width - i + 16);
            if (paramRectangle.x + paramRectangle.width > this.dw)
                paramRectangle.width = (this.dw - paramRectangle.x);
        }
        i = paramRectangle.height % 16;
        if (i != 0) {
            paramRectangle.height = (paramRectangle.height - i + 16);
            if (paramRectangle.y + paramRectangle.height > this.dh)
                paramRectangle.height = (this.dh - paramRectangle.y);
        }
        return paramRectangle;
    }

    private boolean isChangeArea(BufferedImage paramBufferedImage1, BufferedImage paramBufferedImage2, Rectangle paramRectangle) {
        for (int i = 0; i < paramRectangle.width; i++)
            for (int j = 0; j < paramRectangle.height; j++)
                if (paramBufferedImage1.getRGB(i, j) != paramBufferedImage2.getRGB(i, j))
                    return true;
        return false;
    }

    private Rectangle getChangeArea(BufferedImage paramBufferedImage1, BufferedImage paramBufferedImage2, Rectangle paramRectangle) {
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = paramRectangle.width;
        int i1 = paramRectangle.height;
        try {
            for (i = 0; i < paramRectangle.width; i++)
                for (j = 0; j < paramRectangle.height; j++) {
                    if (paramBufferedImage1.getRGB(i, j) == paramBufferedImage2.getRGB(i, j))
                        continue;
                    throw new Exception();
                }
            return null;
        } catch (Exception e1) {
            k = i;
            try {
                for (j = 0; j < paramRectangle.height; j++)
                    for (i = k; i < paramRectangle.width; i++) {
                        if (paramBufferedImage1.getRGB(i, j) == paramBufferedImage2.getRGB(i, j))
                            continue;
                        throw new Exception();
                    }
                return null;
            } catch (Exception e2) {
                m = j;
                try {
                    for (i = paramRectangle.width; i > k; i--)
                        for (j = m; j < paramRectangle.height; j++) {
                            if (paramBufferedImage1.getRGB(i, j) == paramBufferedImage2.getRGB(i, j))
                                continue;
                            throw new Exception();
                        }
                    return null;
                } catch (Exception e3) {
                    n = i;
                    try {
                        for (j = paramRectangle.height; j > m; j--)
                            for (i = n; i > k; i--) {
                                if (paramBufferedImage1.getRGB(i, j) == paramBufferedImage2.getRGB(i, j))
                                    continue;
                                throw new Exception();
                            }
                        return null;
                    } catch (Exception localException4) {
                        i1 = j;
                        if ((n - k > 0) && (i1 - m > 0))
                            return new Rectangle(k, m, n - k, i1 - m);
                    }
                }
            }
        }
        return null;
    }

    public void run() {
        dw = device.getDefaultConfiguration().getBounds().width;
        dh = device.getDefaultConfiguration().getBounds().height;
        int i = dw / 4;
        int j = dh / 4;
        BufferedImage bufferedimage = robot.createScreenCapture(new Rectangle(0, 0, dw, dh));
        for (int k = 0; k < 4; k++) {
            for (int l = 0; l < 4; l++) {
                Rectangle rectangle = new Rectangle();
                rectangle.x = i * k;
                rectangle.y = j * l;
                rectangle.width = i;
                rectangle.height = j;
                rectangle = alignRectangle(rectangle);
                rects[k * 4 + l] = rectangle;
                oldImages[k * 4 + l] = bufferedimage.getSubimage(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            }

        }

        ArrayList arraylist = new ArrayList();
        int i1 = 0;
        Object obj = null;
        try {
            Thread.sleep(30L); //source 300
        } catch (Exception exception) {
        }
        long l1 = System.currentTimeMillis();
        do {
            if (stop)
                break;
            if (defaultPixel == null)
                try {
                    Thread.sleep(100L); // source 1000
                } catch (Exception exception1) {
                }
            else if (times++ > 0)
                try {
                    if (clients.size() > 0) {
                        int j1 = 0;
                        Rectangle rectangle1 = null;
                        do {
                            if (rectRatos[i1] >= 0) {
                                Rectangle rectangle2 = rects[i1];
                                long l2 = System.currentTimeMillis();
                                BufferedImage bufferedimage1 = robot.createScreenCapture(rectangle2);
                                l2 = System.currentTimeMillis();
                                rectangle1 = getChangeArea(oldImages[i1], bufferedimage1, rectangle2);
                                if (rectangle1 != null) {
                                    Iterator iterator = clients.keySet().iterator();
                                    do {
                                        if (!iterator.hasNext())
                                            break;
                                        RFBClient rfbclient = (RFBClient) iterator.next();
                                        RobotClient robotclient = (RobotClient) clients.get(rfbclient);
                                        robotclient.pe = rfbclient.getPreferredEncoding();
                                        robotclient.pf = rfbclient.getPixelFormat();
                                        if (robotclient.pf == null)
                                            robotclient.pf = defaultPixel;
                                        Rect rect = Rect.encode(robotclient.pe, robotclient.pf, bufferedimage1.getSubimage(rectangle1.x, rectangle1.y, rectangle1.width, rectangle1.height), rectangle1.x + rectangle2.x, rectangle1.y + rectangle2.y);
                                        Rect arect[] = {
                                                rect
                                        };
                                        if (arect != null)
                                            try {
                                                rfbclient.writeFrameBufferUpdate(arect);
                                            } catch (SocketException socketexception) {
                                                arraylist.add(rfbclient);
                                            } catch (Exception exception8) {
                                                exception8.printStackTrace();
                                            }
                                    } while (true);
                                    for (int k1 = 0; k1 < arraylist.size(); k1++) {
                                        RFBClient rfbclient1 = (RFBClient) arraylist.get(k1);
                                        removeClient(rfbclient1);
                                    }

                                    arraylist.clear();
                                    try {
                                        Thread.sleep(5L);
                                    } catch (Exception exception7) {
                                    }
                                    j1 += 5;
                                    oldImages[i1] = bufferedimage1;
                                    rectRatos[i1]++;
                                } else {
                                    try {
                                        Thread.sleep(10L);
                                    } catch (Exception exception6) {
                                    }
                                    rectRatos[i1]--;
                                }
                            }
                            i1 = (i1 + 1) % 16;
                            if (i1 == 0) {
                                if (System.currentTimeMillis() - l1 > 3000L) {
                                    l1 = System.currentTimeMillis();
                                    Arrays.fill(rectRatos, 0);
                                }
                                if (j1 == 0)
                                    try {
                                        Thread.sleep(50L); //source 500
                                    } catch (Exception exception5) {
                                    }
                            }
                        } while (rectangle1 == null && i1 != 0);
                    } else {
                        try {
                            Thread.sleep(100L); //source 1000
                        } catch (Exception exception2) {
                        }
                    }
                } catch (Exception exception3) {
                    exception3.printStackTrace();
                } finally {

                }
            else
                try {
                    Thread.sleep(20L);/*source 2000L*/
                } catch (Exception exception4) {
                }
        } while (true);
    }

    public void close() {
        System.out.println("Close robot");
        this.stop = true;
    }
}