package org.i9.slb.platform.anseko.libs.net.rfb;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			KeyCode

public abstract class keysym {

    public static final int XK_space = 32;
    public static final int XK_exclam = 33;
    public static final int XK_quotedbl = 34;
    public static final int XK_numbersign = 35;
    public static final int XK_dollar = 36;
    public static final int XK_percent = 37;
    public static final int XK_ampersand = 38;
    public static final int XK_apostrophe = 39;
    public static final int XK_quoteright = 39;
    public static final int XK_parenleft = 40;
    public static final int XK_parenright = 41;
    public static final int XK_asterisk = 42;
    public static final int XK_plus = 43;
    public static final int XK_comma = 44;
    public static final int XK_minus = 45;
    public static final int XK_period = 46;
    public static final int XK_slash = 47;
    public static final int XK_colon = 58;
    public static final int XK_semicolon = 59;
    public static final int XK_less = 60;
    public static final int XK_equal = 61;
    public static final int XK_greater = 62;
    public static final int XK_question = 63;
    public static final int XK_at = 64;
    public static final int XK_bracketleft = 91;
    public static final int XK_backslash = 92;
    public static final int XK_bracketright = 93;
    public static final int XK_asciicircum = 94;
    public static final int XK_underscore = 95;
    public static final int XK_grave = 96;
    public static final int XK_quoteleft = 96;
    public static final int XK_braceleft = 123;
    public static final int XK_bar = 124;
    public static final int XK_braceright = 125;
    public static final int XK_asciitilde = 126;
    public static final int XK_nobreakspace = 160;
    public static final int XK_exclamdown = 161;
    public static final int XK_cent = 162;
    public static final int XK_sterling = 163;
    public static final int XK_currency = 164;
    public static final int XK_yen = 165;
    public static final int XK_brokenbar = 166;
    public static final int XK_section = 167;
    public static final int XK_diaeresis = 168;
    public static final int XK_copyright = 169;
    public static final int XK_ordfeminine = 170;
    public static final int XK_guillemotleft = 171;
    public static final int XK_notsign = 172;
    public static final int XK_hyphen = 173;
    public static final int XK_registered = 174;
    public static final int XK_macron = 175;
    public static final int XK_degree = 176;
    public static final int XK_plusminus = 177;
    public static final int XK_twosuperior = 178;
    public static final int XK_threesuperior = 179;
    public static final int XK_acute = 180;
    public static final int XK_mu = 181;
    public static final int XK_paragraph = 182;
    public static final int XK_periodcentered = 183;
    public static final int XK_cedilla = 184;
    public static final int XK_onesuperior = 185;
    public static final int XK_masculine = 186;
    public static final int XK_guillemotright = 187;
    public static final int XK_onequarter = 188;
    public static final int XK_onehalf = 189;
    public static final int XK_threequarters = 190;
    public static final int XK_questiondown = 191;
    public static final int XK_Agrave = 192;
    public static final int XK_Aacute = 193;
    public static final int XK_Acircumflex = 194;
    public static final int XK_Atilde = 195;
    public static final int XK_Adiaeresis = 196;
    public static final int XK_Aring = 197;
    public static final int XK_AE = 198;
    public static final int XK_Ccedilla = 199;
    public static final int XK_Egrave = 200;
    public static final int XK_Eacute = 201;
    public static final int XK_Ecircumflex = 202;
    public static final int XK_Ediaeresis = 203;
    public static final int XK_Igrave = 204;
    public static final int XK_Iacute = 205;
    public static final int XK_Icircumflex = 206;
    public static final int XK_Idiaeresis = 207;
    public static final int XK_ETH = 208;
    public static final int XK_Eth = 208;
    public static final int XK_Ntilde = 209;
    public static final int XK_Ograve = 210;
    public static final int XK_Oacute = 211;
    public static final int XK_Ocircumflex = 212;
    public static final int XK_Otilde = 213;
    public static final int XK_Odiaeresis = 214;
    public static final int XK_multiply = 215;
    public static final int XK_Ooblique = 216;
    public static final int XK_Ugrave = 217;
    public static final int XK_Uacute = 218;
    public static final int XK_Ucircumflex = 219;
    public static final int XK_Udiaeresis = 220;
    public static final int XK_Yacute = 221;
    public static final int XK_THORN = 222;
    public static final int XK_Thorn = 222;
    public static final int XK_ssharp = 223;
    public static final int XK_agrave = 224;
    public static final int XK_aacute = 225;
    public static final int XK_acircumflex = 226;
    public static final int XK_atilde = 227;
    public static final int XK_adiaeresis = 228;
    public static final int XK_aring = 229;
    public static final int XK_ae = 230;
    public static final int XK_ccedilla = 231;
    public static final int XK_egrave = 232;
    public static final int XK_eacute = 233;
    public static final int XK_ecircumflex = 234;
    public static final int XK_ediaeresis = 235;
    public static final int XK_igrave = 236;
    public static final int XK_iacute = 237;
    public static final int XK_icircumflex = 238;
    public static final int XK_idiaeresis = 239;
    public static final int XK_eth = 240;
    public static final int XK_ntilde = 241;
    public static final int XK_ograve = 242;
    public static final int XK_oacute = 243;
    public static final int XK_ocircumflex = 244;
    public static final int XK_otilde = 245;
    public static final int XK_odiaeresis = 246;
    public static final int XK_division = 247;
    public static final int XK_oslash = 248;
    public static final int XK_ugrave = 249;
    public static final int XK_uacute = 250;
    public static final int XK_ucircumflex = 251;
    public static final int XK_udiaeresis = 252;
    public static final int XK_yacute = 253;
    public static final int XK_thorn = 254;
    public static final int XK_ydiaeresis = 255;
    public static final int samekeys[] = {
            32, 44, 45, 46, 47
    };
    public static final int DeadGrave = 65104;
    public static final int DeadAcute = 65105;
    public static final int DeadCircumflex = 65106;
    public static final int DeadTilde = 65107;
    public static final int BackSpace = 65288;
    public static final int Tab = 65289;
    public static final int Linefeed = 65290;
    public static final int Clear = 65291;
    public static final int Return = 65293;
    public static final int Pause = 65299;
    public static final int ScrollLock = 65300;
    public static final int SysReq = 65301;
    public static final int Escape = 65307;
    public static final int Delete = 65535;
    public static final int Home = 65360;
    public static final int Left = 65361;
    public static final int Up = 65362;
    public static final int Right = 65363;
    public static final int Down = 65364;
    public static final int PageUp = 65365;
    public static final int PageDown = 65366;
    public static final int End = 65367;
    public static final int Begin = 65368;
    public static final int Select = 65376;
    public static final int Print = 65377;
    public static final int Execute = 65378;
    public static final int Insert = 65379;
    public static final int Cancel = 65385;
    public static final int Help = 65386;
    public static final int Break = 65387;
    public static final int NumLock = 65391;
    public static final int KpSpace = 65408;
    public static final int KpTab = 65417;
    public static final int KpEnter = 65421;
    public static final int KpHome = 65429;
    public static final int KpLeft = 65430;
    public static final int KpUp = 65431;
    public static final int KpRight = 65432;
    public static final int KpDown = 65433;
    public static final int KpPrior = 65434;
    public static final int KpPageUp = 65434;
    public static final int KpNext = 65435;
    public static final int KpPageDown = 65435;
    public static final int KpEnd = 65436;
    public static final int KpBegin = 65437;
    public static final int KpInsert = 65438;
    public static final int KpDelete = 65439;
    public static final int KpEqual = 65469;
    public static final int KpMultiply = 65450;
    public static final int KpAdd = 65451;
    public static final int KpSeparator = 65452;
    public static final int KpSubtract = 65453;
    public static final int KpDecimal = 65454;
    public static final int KpDivide = 65455;
    public static final int KpF1 = 65425;
    public static final int KpF2 = 65426;
    public static final int KpF3 = 65427;
    public static final int KpF4 = 65428;
    public static final int Kp0 = 65456;
    public static final int Kp1 = 65457;
    public static final int Kp2 = 65458;
    public static final int Kp3 = 65459;
    public static final int Kp4 = 65460;
    public static final int Kp5 = 65461;
    public static final int Kp6 = 65462;
    public static final int Kp7 = 65463;
    public static final int Kp8 = 65464;
    public static final int Kp9 = 65465;
    public static final int F1 = 65470;
    public static final int F2 = 65471;
    public static final int F3 = 65472;
    public static final int F4 = 65473;
    public static final int F5 = 65474;
    public static final int F6 = 65475;
    public static final int F7 = 65476;
    public static final int F8 = 65477;
    public static final int F9 = 65478;
    public static final int F10 = 65479;
    public static final int F11 = 65480;
    public static final int F12 = 65481;
    public static final int F13 = 65482;
    public static final int F14 = 65483;
    public static final int F15 = 65484;
    public static final int F16 = 65485;
    public static final int F17 = 65486;
    public static final int F18 = 65487;
    public static final int F19 = 65488;
    public static final int F20 = 65489;
    public static final int F21 = 65490;
    public static final int F22 = 65491;
    public static final int F23 = 65492;
    public static final int F24 = 65493;
    public static final int ShiftL = 65505;
    public static final int ShiftR = 65506;
    public static final int ControlL = 65507;
    public static final int ControlR = 65508;
    public static final int CapsLock = 65509;
    public static final int ShiftLock = 65510;
    public static final int MetaL = 65511;
    public static final int MetaR = 65512;
    public static final int AltL = 65513;
    public static final int AltR = 65514;

    public keysym() {
    }

    public static int toVK(int i) {
        switch (i) {
            case 65104:
                return 128;

            case 65105:
                return 129;

            case 65106:
                return 130;

            case 65107:
                return 131;

            case 65288:
                return 8;

            case 65289:
                return 9;

            case 65291:
                return 12;

            case 65293:
                return 10;

            case 65299:
                return 19;

            case 65300:
                return 145;

            case 65307:
                return 27;

            case 65535:
                return 127;

            case 65360:
                return 36;

            case 65361:
                return 37;

            case 65362:
                return 38;

            case 65363:
                return 39;

            case 65364:
                return 40;

            case 65365:
                return 33;

            case 65366:
                return 34;

            case 65367:
                return 35;

            case 65377:
                return 154;

            case 65379:
                return 155;

            case 65385:
                return 3;

            case 65386:
                return 156;

            case 65391:
                return 144;

            case 65408:
                return 32;

            case 65417:
                return 9;

            case 65421:
                return 10;

            case 65429:
                return 36;

            case 65430:
                return 37;

            case 65431:
                return 38;

            case 65432:
                return 39;

            case 65433:
                return 40;

            case 65434:
                return 33;

            case 65435:
                return 34;

            case 65436:
                return 35;

            case 65438:
                return 155;

            case 65439:
                return 127;

            case 65469:
                return 61;

            case 65450:
                return 106;

            case 65451:
                return 107;

            case 65452:
                return 108;

            case 65453:
                return 109;

            case 65454:
                return 110;

            case 65455:
                return 111;

            case 65425:
                return 112;

            case 65426:
                return 113;

            case 65427:
                return 114;

            case 65428:
                return 115;

            case 65456:
                return 96;

            case 65457:
                return 97;

            case 65458:
                return 98;

            case 65459:
                return 99;

            case 65460:
                return 100;

            case 65461:
                return 101;

            case 65462:
                return 102;

            case 65463:
                return 103;

            case 65464:
                return 104;

            case 65465:
                return 105;

            case 65470:
                return 112;

            case 65471:
                return 113;

            case 65472:
                return 114;

            case 65473:
                return 115;

            case 65474:
                return 116;

            case 65475:
                return 117;

            case 65476:
                return 118;

            case 65477:
                return 119;

            case 65478:
                return 120;

            case 65479:
                return 121;

            case 65480:
                return 122;

            case 65481:
                return 123;

            case 65482:
                return 123;

            case 65483:
                return 123;

            case 65484:
                return 123;

            case 65485:
                return 123;

            case 65486:
                return 123;

            case 65487:
                return 123;

            case 65488:
                return 123;

            case 65489:
                return 123;

            case 65490:
                return 123;

            case 65491:
                return 123;

            case 65492:
                return 123;

            case 65493:
                return 123;

            case 65509:
                return 20;

            case 32: // ' '
                return 32;

            case 33: // '!'
                return 517;

            case 34: // '"'
                return 152;

            case 35: // '#'
                return 520;

            case 36: // '$'
                return 515;

            case 38: // '&'
                return 150;

            case 39: // '\''
                return 222;

            case 40: // '('
                return 519;

            case 41: // ')'
                return 522;

            case 42: // '*'
                return 151;

            case 44: // ','
                return 44;

            case 45: // '-'
                return 45;

            case 46: // '.'
                return 46;

            case 47: // '/'
                return 47;

            case 59: // ';'
                return 59;

            case 61: // '='
                return 61;

            case 64: // '@'
                return 512;

            case 91: // '['
                return 91;

            case 92: // '\\'
                return 92;

            case 93: // ']'
                return 93;

            case 94: // '^'
                return 514;

            case 95: // '_'
                return 523;

            case 124: // '|'
                return 108;
        }
        return 0;
    }

    public static KeyCode toVKCode(int i) {
        KeyCode keycode = null;
        boolean flag = false;
        int j = 0;
        if (i >= 65 && i <= 90) {
            j = i;
            flag = true;
        } else if (i >= 48 && i <= 57)
            j = i;
        else if (i >= 97 && i <= 122)
            j = (i - 97) + 65;
        else
            switch (i) {
                case 60: // '<'
                    j = 44;
                    flag = true;
                    break;

                case 62: // '>'
                    j = 46;
                    flag = true;
                    break;

                case 63: // '?'
                    j = 47;
                    flag = true;
                    break;

                case 58: // ':'
                    j = 59;
                    flag = true;
                    break;

                case 34: // '"'
                    j = 222;
                    flag = true;
                    break;

                case 33: // '!'
                    j = 49;
                    flag = true;
                    break;

                case 64: // '@'
                    j = 50;
                    flag = true;
                    break;

                case 35: // '#'
                    j = 51;
                    flag = true;
                    break;

                case 36: // '$'
                    j = 52;
                    flag = true;
                    break;

                case 37: // '%'
                    j = 53;
                    flag = true;
                    break;

                case 38: // '&'
                    j = 55;
                    flag = true;
                    break;

                case 94: // '^'
                    j = 54;
                    flag = true;
                    break;

                case 40: // '('
                    j = 57;
                    flag = true;
                    break;

                case 41: // ')'
                    j = 48;
                    flag = true;
                    break;

                case 42: // '*'
                    j = 56;
                    flag = true;
                    break;

                case 43: // '+'
                    j = 61;
                    flag = true;
                    break;

                case 95: // '_'
                    j = 45;
                    flag = true;
                    break;

                case 123: // '{'
                    j = 91;
                    flag = true;
                    break;

                case 125: // '}'
                    j = 93;
                    flag = true;
                    break;

                default:
                    j = toVKall(i);
                    break;
            }
        if (j != 0) {
            keycode = new KeyCode();
            keycode.key = j;
            keycode.isShift = flag;
        }
        return keycode;
    }

    public static int toVKall(int i) {
        int j = toVK(i);
        if (j != 0)
            return j;
        switch (i) {
            case 65505:
                return 16;

            case 65506:
                return 16;

            case 65507:
                return 17;

            case 65508:
                return 17;

            case 65511:
                return 157;

            case 65512:
                return 157;

            case 65513:
                return 18;

            case 65514:
                return 18;

            case 65509:
            case 65510:
            default:
                return 0;
        }
    }

    public static int toMask(int i) {
        switch (i) {
            case 65505:
                return 1;

            case 65506:
                return 1;

            case 65507:
                return 2;

            case 65508:
                return 2;

            case 65511:
                return 4;

            case 65512:
                return 4;

            case 65513:
                return 8;

            case 65514:
                return 8;

            case 65509:
            case 65510:
            default:
                return 0;
        }
    }

}
